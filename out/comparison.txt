# Hyperparameters
batch_size = 32
steps_per_epoch = 160
epochs = 9
hidden_units = 1024
learning_rate = 0.00005
dropout = 0.4

D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/analyticsvidhya/inception.py
Started execution
Starting extraction
Completed extraction
Found 5227 images belonging to 4 classes.
Found 1588 images belonging to 4 classes.
2021-09-05 17:58:54.814161: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX AVX2
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-09-05 17:58:55.537931: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1510] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 2776 MB memory:  -> device: 0, name: GeForce GTX 1050 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1
2021-09-05 17:58:59.069375: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:185] None of the MLIR Optimization Passes are enabled (registered 2)
Epoch 1/9
2021-09-05 17:59:05.887964: I tensorflow/stream_executor/cuda/cuda_dnn.cc:369] Loaded cuDNN version 8202
160/160 [==============================] - 67s 358ms/step - loss: 1.1512 - acc: 0.6939 - val_loss: 1.5812 - val_acc: 0.5063
Epoch 2/9
160/160 [==============================] - 49s 309ms/step - loss: 0.6186 - acc: 0.7858 - val_loss: 1.8413 - val_acc: 0.4414
Epoch 3/9
160/160 [==============================] - 47s 294ms/step - loss: 0.5661 - acc: 0.8045 - val_loss: 1.6595 - val_acc: 0.4981
Epoch 4/9
160/160 [==============================] - 46s 287ms/step - loss: 0.5346 - acc: 0.8200 - val_loss: 1.4966 - val_acc: 0.5233
Epoch 5/9
160/160 [==============================] - 45s 280ms/step - loss: 0.4800 - acc: 0.8364 - val_loss: 1.5671 - val_acc: 0.5220
Epoch 6/9
160/160 [==============================] - 45s 279ms/step - loss: 0.4747 - acc: 0.8431 - val_loss: 1.3612 - val_acc: 0.5674
Epoch 7/9
160/160 [==============================] - 45s 283ms/step - loss: 0.4603 - acc: 0.8460 - val_loss: 1.6235 - val_acc: 0.5309
Epoch 8/9
160/160 [==============================] - 44s 277ms/step - loss: 0.4391 - acc: 0.8517 - val_loss: 1.8746 - val_acc: 0.4924
Epoch 9/9
160/160 [==============================] - 44s 276ms/step - loss: 0.4195 - acc: 0.8645 - val_loss: 1.5189 - val_acc: 0.5554
2021-09-05 18:06:30.324352: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.

Process finished with exit code 0

D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/analyticsvidhya/inception.py
Started execution
Starting extraction
Completed extraction
Found 5227 images belonging to 4 classes.
Found 1588 images belonging to 4 classes.
2021-09-05 18:13:42.232299: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX AVX2
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-09-05 18:13:42.849948: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1510] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 2776 MB memory:  -> device: 0, name: GeForce GTX 1050 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1
Batch size: 32
Steps / Epoch: 160
Epochs: 9
Hidden units: 1024
Learning rate: 1e-05
Dropout: 0.5
2021-09-05 18:13:46.177026: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:185] None of the MLIR Optimization Passes are enabled (registered 2)
Epoch 1/9
2021-09-05 18:13:51.305080: I tensorflow/stream_executor/cuda/cuda_dnn.cc:369] Loaded cuDNN version 8202
160/160 [==============================] - 56s 302ms/step - loss: 1.0680 - acc: 0.6352 - val_loss: 1.2506 - val_acc: 0.5441
Epoch 2/9
160/160 [==============================] - 42s 261ms/step - loss: 0.6645 - acc: 0.7518 - val_loss: 1.3172 - val_acc: 0.5126
Epoch 3/9
160/160 [==============================] - 42s 260ms/step - loss: 0.5750 - acc: 0.7866 - val_loss: 1.3744 - val_acc: 0.5290
Epoch 4/9
160/160 [==============================] - 43s 268ms/step - loss: 0.5290 - acc: 0.8007 - val_loss: 1.3270 - val_acc: 0.5460
Epoch 5/9
160/160 [==============================] - 42s 264ms/step - loss: 0.4920 - acc: 0.8164 - val_loss: 1.3911 - val_acc: 0.5384
Epoch 6/9
160/160 [==============================] - 42s 261ms/step - loss: 0.4656 - acc: 0.8282 - val_loss: 1.4052 - val_acc: 0.5378
Epoch 7/9
160/160 [==============================] - 41s 257ms/step - loss: 0.4450 - acc: 0.8408 - val_loss: 1.2413 - val_acc: 0.5623
Epoch 8/9
160/160 [==============================] - 41s 258ms/step - loss: 0.4339 - acc: 0.8355 - val_loss: 1.3290 - val_acc: 0.5598
Epoch 9/9
160/160 [==============================] - 41s 259ms/step - loss: 0.4271 - acc: 0.8417 - val_loss: 1.3342 - val_acc: 0.5510
2021-09-05 18:20:33.602789: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.

Process finished with exit code 0


D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/analyticsvidhya/inception.py
Started execution
Starting extraction
Completed extraction
Found 3259 images belonging to 2 classes.
Found 962 images belonging to 2 classes.
2021-09-05 22:22:49.529607: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX AVX2
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-09-05 22:22:50.148002: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1510] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 2776 MB memory:  -> device: 0, name: GeForce GTX 1050 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1
Batch size: 20
Steps / Epoch: 100
Epochs: 15
Hidden units: 128
Learning rate: 1e-05
Dropout: 0.5
Number of classes: 2
2021-09-05 22:22:52.983850: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:185] None of the MLIR Optimization Passes are enabled (registered 2)
Epoch 1/15
2021-09-05 22:22:57.873440: I tensorflow/stream_executor/cuda/cuda_dnn.cc:369] Loaded cuDNN version 8202
100/100 [==============================] - 28s 211ms/step - loss: 0.9787 - acc: 0.6200 - val_loss: 0.5917 - val_acc: 0.7183
Epoch 2/15
100/100 [==============================] - 18s 178ms/step - loss: 0.6911 - acc: 0.7335 - val_loss: 0.4584 - val_acc: 0.7796
Epoch 3/15
100/100 [==============================] - 19s 192ms/step - loss: 0.5697 - acc: 0.7824 - val_loss: 0.4367 - val_acc: 0.7921
Epoch 4/15
100/100 [==============================] - 19s 189ms/step - loss: 0.5163 - acc: 0.8130 - val_loss: 0.3111 - val_acc: 0.8659
Epoch 5/15
100/100 [==============================] - 18s 181ms/step - loss: 0.4167 - acc: 0.8369 - val_loss: 0.3137 - val_acc: 0.8628
Epoch 6/15
100/100 [==============================] - 17s 174ms/step - loss: 0.3650 - acc: 0.8604 - val_loss: 0.2579 - val_acc: 0.8981
Epoch 7/15
100/100 [==============================] - 18s 180ms/step - loss: 0.3230 - acc: 0.8825 - val_loss: 0.2534 - val_acc: 0.8960
Epoch 8/15
100/100 [==============================] - 18s 177ms/step - loss: 0.2807 - acc: 0.9025 - val_loss: 0.2362 - val_acc: 0.9023
Epoch 9/15
100/100 [==============================] - 18s 180ms/step - loss: 0.2947 - acc: 0.8954 - val_loss: 0.2326 - val_acc: 0.8992
Epoch 10/15
100/100 [==============================] - 19s 185ms/step - loss: 0.2678 - acc: 0.9055 - val_loss: 0.2082 - val_acc: 0.9314
Epoch 11/15
100/100 [==============================] - 17s 173ms/step - loss: 0.2412 - acc: 0.9165 - val_loss: 0.2042 - val_acc: 0.9189
Epoch 12/15
100/100 [==============================] - 18s 176ms/step - loss: 0.2316 - acc: 0.9250 - val_loss: 0.2020 - val_acc: 0.9148
Epoch 13/15
100/100 [==============================] - 17s 173ms/step - loss: 0.2290 - acc: 0.9225 - val_loss: 0.1916 - val_acc: 0.9324
Epoch 14/15
100/100 [==============================] - 18s 177ms/step - loss: 0.2147 - acc: 0.9235 - val_loss: 0.1924 - val_acc: 0.9241
Epoch 15/15
100/100 [==============================] - 18s 176ms/step - loss: 0.2150 - acc: 0.9340 - val_loss: 0.1968 - val_acc: 0.9220
2021-09-05 22:27:50.700334: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.

Process finished with exit code 0

D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/analyticsvidhya/inception.py
Started execution
Starting extraction: 2-classes-20210905-154028
Completed extraction: 2-classes-20210905-154028
Found 2693 images belonging to 2 classes.
Found 845 images belonging to 2 classes.
2021-09-05 22:32:19.749419: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX AVX2
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-09-05 22:32:20.432368: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1510] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 2776 MB memory:  -> device: 0, name: GeForce GTX 1050 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1
Batch size: 20
Steps / Epoch: 100
Epochs: 15
Hidden units: 128
Learning rate: 1e-05
Dropout: 0.5
Number of classes: 2
2021-09-05 22:32:23.896813: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:185] None of the MLIR Optimization Passes are enabled (registered 2)
Epoch 1/15
2021-09-05 22:32:31.274310: I tensorflow/stream_executor/cuda/cuda_dnn.cc:369] Loaded cuDNN version 8202
100/100 [==============================] - 36s 265ms/step - loss: 1.0569 - acc: 0.6005 - val_loss: 0.6940 - val_acc: 0.6556
Epoch 2/15
100/100 [==============================] - 22s 220ms/step - loss: 0.5686 - acc: 0.7812 - val_loss: 0.6341 - val_acc: 0.7077
Epoch 3/15
100/100 [==============================] - 21s 213ms/step - loss: 0.4596 - acc: 0.8344 - val_loss: 0.6214 - val_acc: 0.7361
Epoch 4/15
100/100 [==============================] - 21s 208ms/step - loss: 0.3646 - acc: 0.8736 - val_loss: 0.5836 - val_acc: 0.7598
Epoch 5/15
100/100 [==============================] - 20s 195ms/step - loss: 0.2957 - acc: 0.8986 - val_loss: 0.5383 - val_acc: 0.7941
Epoch 6/15
100/100 [==============================] - 20s 200ms/step - loss: 0.2827 - acc: 0.8971 - val_loss: 0.5708 - val_acc: 0.7917
Epoch 7/15
100/100 [==============================] - 21s 211ms/step - loss: 0.2280 - acc: 0.9242 - val_loss: 0.4909 - val_acc: 0.8130
Epoch 8/15
100/100 [==============================] - 19s 192ms/step - loss: 0.2324 - acc: 0.9225 - val_loss: 0.4902 - val_acc: 0.8225
Epoch 9/15
100/100 [==============================] - 19s 194ms/step - loss: 0.1857 - acc: 0.9398 - val_loss: 0.4677 - val_acc: 0.8213
Epoch 10/15
100/100 [==============================] - 20s 198ms/step - loss: 0.1838 - acc: 0.9350 - val_loss: 0.4718 - val_acc: 0.8237
Epoch 11/15
100/100 [==============================] - 19s 194ms/step - loss: 0.1803 - acc: 0.9348 - val_loss: 0.4534 - val_acc: 0.8284
Epoch 12/15
100/100 [==============================] - 20s 200ms/step - loss: 0.1597 - acc: 0.9483 - val_loss: 0.4360 - val_acc: 0.8343
Epoch 13/15
100/100 [==============================] - 20s 198ms/step - loss: 0.1726 - acc: 0.9453 - val_loss: 0.4166 - val_acc: 0.8426
Epoch 14/15
100/100 [==============================] - 20s 203ms/step - loss: 0.1294 - acc: 0.9568 - val_loss: 0.4069 - val_acc: 0.8473
Epoch 15/15
100/100 [==============================] - 20s 204ms/step - loss: 0.1343 - acc: 0.9508 - val_loss: 0.4895 - val_acc: 0.8166
2021-09-05 22:38:04.498475: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.

D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/analyticsvidhya/inception.py
Started execution
Starting extraction: 27-classes-20210906-210926
Completed extraction: 27-classes-20210906-210926
Found 8493 images belonging to 19 classes.
Found 2067 images belonging to 19 classes.
2021-09-07 19:05:33.183315: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX AVX2
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-09-07 19:05:41.173737: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1510] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 2776 MB memory:  -> device: 0, name: GeForce GTX 1050 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1
Batch size: 20
Steps / Epoch: 100
Epochs: 15
Hidden units: 128
Learning rate: 1e-05
Dropout: 0.5
Number of classes: 19
2021-09-07 19:05:46.386539: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:185] None of the MLIR Optimization Passes are enabled (registered 2)
Epoch 1/15
2021-09-07 19:06:00.750336: I tensorflow/stream_executor/cuda/cuda_dnn.cc:369] Loaded cuDNN version 8202
100/100 [==============================] - 64s 309ms/step - loss: 4.3730 - acc: 0.0710 - val_loss: 3.2451 - val_acc: 0.0793
Epoch 2/15
100/100 [==============================] - 29s 292ms/step - loss: 4.0861 - acc: 0.0735 - val_loss: 3.1920 - val_acc: 0.0958
Epoch 3/15
100/100 [==============================] - 27s 266ms/step - loss: 3.8931 - acc: 0.1029 - val_loss: 3.1621 - val_acc: 0.0972
Epoch 4/15
100/100 [==============================] - 26s 257ms/step - loss: 3.8135 - acc: 0.1110 - val_loss: 3.2157 - val_acc: 0.1016
Epoch 5/15
100/100 [==============================] - 22s 222ms/step - loss: 3.7498 - acc: 0.1215 - val_loss: 3.1683 - val_acc: 0.1137
Epoch 6/15
100/100 [==============================] - 22s 221ms/step - loss: 3.5428 - acc: 0.1375 - val_loss: 3.1761 - val_acc: 0.1069
Epoch 7/15
100/100 [==============================] - 21s 213ms/step - loss: 3.4435 - acc: 0.1595 - val_loss: 3.2072 - val_acc: 0.1118
Epoch 8/15
100/100 [==============================] - 21s 215ms/step - loss: 3.4768 - acc: 0.1565 - val_loss: 3.1657 - val_acc: 0.1243
Epoch 9/15
100/100 [==============================] - 23s 230ms/step - loss: 3.3136 - acc: 0.1865 - val_loss: 3.1958 - val_acc: 0.1137
Epoch 10/15
100/100 [==============================] - 21s 209ms/step - loss: 3.3522 - acc: 0.1800 - val_loss: 3.2286 - val_acc: 0.1142
Epoch 11/15
100/100 [==============================] - 22s 221ms/step - loss: 3.3495 - acc: 0.1815 - val_loss: 3.1769 - val_acc: 0.1195
Epoch 12/15
100/100 [==============================] - 21s 211ms/step - loss: 3.2552 - acc: 0.1885 - val_loss: 3.1811 - val_acc: 0.1229
Epoch 13/15
100/100 [==============================] - 22s 216ms/step - loss: 3.2025 - acc: 0.1920 - val_loss: 3.2119 - val_acc: 0.1263
Epoch 14/15
100/100 [==============================] - 22s 222ms/step - loss: 3.0732 - acc: 0.2180 - val_loss: 3.2537 - val_acc: 0.1137
Epoch 15/15
100/100 [==============================] - 22s 221ms/step - loss: 3.0852 - acc: 0.2185 - val_loss: 3.2313 - val_acc: 0.1311
2021-09-07 19:12:30.375705: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.

Process finished with exit code 0

D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/analyticsvidhya/inception.py
Started execution
Starting extraction: 27-classes-20210906-210926
Completed extraction: 27-classes-20210906-210926
Found 17151 images belonging to 19 classes.
Found 2067 images belonging to 19 classes.
2021-09-07 21:19:53.248349: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX AVX2
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-09-07 21:19:53.871438: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1510] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 2776 MB memory:  -> device: 0, name: GeForce GTX 1050 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1
Batch size: 20
Steps / Epoch: 100
Epochs: 15
Hidden units: 1024
Learning rate: 0.0001
Dropout: 0.5
Number of classes: 19
2021-09-07 21:19:56.818793: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:185] None of the MLIR Optimization Passes are enabled (registered 2)
Epoch 1/15
2021-09-07 21:20:02.360658: I tensorflow/stream_executor/cuda/cuda_dnn.cc:369] Loaded cuDNN version 8202
100/100 [==============================] - 33s 257ms/step - loss: 3.9514 - acc: 0.1200 - val_loss: 3.5566 - val_acc: 0.1195
Epoch 2/15
100/100 [==============================] - 21s 214ms/step - loss: 3.3739 - acc: 0.1955 - val_loss: 3.4838 - val_acc: 0.1248
Epoch 3/15
100/100 [==============================] - 21s 214ms/step - loss: 3.1666 - acc: 0.2235 - val_loss: 3.3730 - val_acc: 0.1272
Epoch 4/15
100/100 [==============================] - 22s 216ms/step - loss: 2.9560 - acc: 0.2800 - val_loss: 3.6013 - val_acc: 0.1268
Epoch 5/15
100/100 [==============================] - 22s 218ms/step - loss: 2.8979 - acc: 0.2880 - val_loss: 3.8658 - val_acc: 0.1432
Epoch 6/15
100/100 [==============================] - 22s 221ms/step - loss: 2.7673 - acc: 0.3094 - val_loss: 3.6491 - val_acc: 0.1606
Epoch 7/15
100/100 [==============================] - 21s 208ms/step - loss: 2.7623 - acc: 0.2955 - val_loss: 3.6792 - val_acc: 0.1335
Epoch 8/15
100/100 [==============================] - 21s 209ms/step - loss: 2.7782 - acc: 0.2915 - val_loss: 3.8376 - val_acc: 0.1214
Epoch 9/15
100/100 [==============================] - 21s 213ms/step - loss: 2.6324 - acc: 0.3355 - val_loss: 3.9297 - val_acc: 0.1355
Epoch 10/15
100/100 [==============================] - 21s 208ms/step - loss: 2.6339 - acc: 0.3235 - val_loss: 3.8417 - val_acc: 0.1364
Epoch 11/15
100/100 [==============================] - 21s 207ms/step - loss: 2.7607 - acc: 0.3255 - val_loss: 3.9538 - val_acc: 0.1301
Epoch 12/15
100/100 [==============================] - 21s 206ms/step - loss: 2.5940 - acc: 0.3330 - val_loss: 3.7975 - val_acc: 0.1485
Epoch 13/15
100/100 [==============================] - 20s 204ms/step - loss: 2.6161 - acc: 0.3340 - val_loss: 3.9833 - val_acc: 0.1674
Epoch 14/15
100/100 [==============================] - 20s 204ms/step - loss: 2.5302 - acc: 0.3555 - val_loss: 4.0938 - val_acc: 0.1408
Epoch 15/15
100/100 [==============================] - 20s 203ms/step - loss: 2.5724 - acc: 0.3465 - val_loss: 4.3143 - val_acc: 0.1403
2021-09-07 21:25:42.460942: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.

Process finished with exit code 0
