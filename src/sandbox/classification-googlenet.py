import json

import torch
import torchvision.transforms as transforms
from PIL import Image

from googlenet_pytorch import GoogLeNet

# Open image


input_image1 = Image.open("..\out\images\caucasian-male1-youtube-_hjfQ41LF0w-20210808-100903\left-00001.png")
input_image2 = Image.open("..\out\images\caucasian-male1-youtube-_hjfQ41LF0w-20210808-100903\left-00002.png")
input_image3 = Image.open("..\out\images\caucasian-male1-youtube-_hjfQ41LF0w-20210808-100903\left-00003.png")
new_img = Image.blend(input_image1, input_image2, 0.5)
input_image = Image.blend(new_img, input_image3, 0.5)
# input_image = Image.open("..\input\images\gan1.jpg")

# Preprocess image
preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])
input_tensor = preprocess(input_image)
input_batch = input_tensor.unsqueeze(0)  # create a mini-batch as expected by the model

# Load class names
labels_map = json.load(open("labels_map.json"))
labels_map = [labels_map[str(i)] for i in range(1000)]

# Classify with GoogLeNet
model = GoogLeNet.from_pretrained("googlenet")
model.eval()

# move the input and model to GPU for speed if available
if torch.cuda.is_available():
    input_batch = input_batch.to("cuda")
    model.to("cuda")

with torch.no_grad():
    logits = model(input_batch)
preds = torch.topk(logits, k=5).indices.squeeze(0).tolist()

print("-----")
for idx in preds:
    label = labels_map[idx]
    prob = torch.softmax(logits, dim=1)[0, idx].item()
    print(f"{label:<75} ({prob * 100:.2f}%)")
