from PIL import Image
import numpy
from tensorflow import keras
from src.constants import *

image_paths = [
    IMAGES_OUTPUT_DIR + '01-asian-female-20210905-230429/classified/CCC/asfe-right-01562-0.png',
    IMAGES_OUTPUT_DIR + '01-asian-female-20210905-230429/classified/OOO/asfe-left-00092-0.png'
]

samples_to_predict = []
for image_path in image_paths:
    image = Image.open(image_path)
    image = image.resize((150, 150))
    image_as_array = numpy.array(image)
    samples_to_predict.append(image_as_array)

samples_to_predict = numpy.array(samples_to_predict)

model = keras.models.load_model(MODEL_OUTPUT_DIR + 'inception-v3-20210907-222341.model')

predictions = model.predict(samples_to_predict)
print(predictions)

classes = numpy.argmax(predictions, axis=1)
print(classes)
