from main import *
from datetime import datetime

# 2. Inception

# Hyperparameters
batch_size = 20
steps_per_epoch = 100
epochs = 15
hidden_units = 1024
learning_rate = 0.0001
dropout = 0.5

# Step 1: Data Augmentation

# Add our data-augmentation parameters to ImageDataGenerator
train_datagen = ImageDataGenerator(rescale=1. / 255., rotation_range=40, width_shift_range=0.2, height_shift_range=0.2,
                                   shear_range=0.2, zoom_range=0.2, horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1.0 / 255.)

# Step 2: Training and Validation Generators
train_generator = train_datagen.flow_from_directory(training_dir, batch_size=batch_size, class_mode='categorical',
                                                    target_size=(150, 150))
validation_generator = test_datagen.flow_from_directory(validation_dir, batch_size=batch_size, class_mode='categorical',
                                                        target_size=(150, 150))

# Step 3: Loading the Base Model
from tensorflow.keras.applications.inception_v3 import InceptionV3

base_model = InceptionV3(input_shape=(150, 150, 3), include_top=False, weights='imagenet')

# Step 4: Compile and Fit
for layer in base_model.layers:
    layer.trainable = False

# We perform the following operations:
# - Flatten the output of our base model to 1 dimension
# - Add a fully connected layer with 1,024 hidden units and ReLU activation
# - This time, we will go with a dropout rate of 0.2
# - Add a final Fully Connected Sigmoid Layer
# - We will again use RMSProp, though you can try out the Adam Optimiser too
from tensorflow.keras.optimizers import RMSprop

x = layers.Flatten()(base_model.output)
# x = layers.Dense(hidden_units, activation='relu')(x)
x = layers.Dropout(dropout)(x)
x = layers.Dense(number_of_classes, activation='softmax')(x)

model = tf.keras.models.Model(base_model.input, x)

model.compile(optimizer=RMSprop(learning_rate=learning_rate), loss='categorical_crossentropy', metrics=['acc'])

print("Batch size: " + str(batch_size))
print("Steps / Epoch: " + str(steps_per_epoch))
print("Epochs: " + str(epochs))
print("Hidden units: " + str(hidden_units))
print("Learning rate: " + str(learning_rate))
print("Dropout: " + str(dropout))
print("Number of classes: " + str(number_of_classes))

inc_history = model.fit(train_generator, validation_data=validation_generator, steps_per_epoch=steps_per_epoch, epochs=epochs)
model.
dateTimeObj = datetime.now()
timestamp = dateTimeObj.strftime('%Y%m%d-%H%M%S')
model_name = 'inception-v3-' + timestamp
model.save('../' + MODEL_OUTPUT_DIR + model_name + '.model')
