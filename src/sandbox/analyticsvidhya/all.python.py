# Import
# ----------------------------------------------------------------------------------------------------------------------
import os
import zipfile
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import layers
from tensorflow.keras import Model
import matplotlib.pyplot as plt

# Preparing the Dataset
# ----------------------------------------------------------------------------------------------------------------------
local_zip = 'tmp/open_close_20210815-1949.zip'
zip_ref = zipfile.ZipFile(local_zip, 'r')
zip_ref.extractall('/tmp')
zip_ref.close()

base_dir = '/tmp/open_close_20210815-1949'
train_dir = os.path.join(base_dir, 'train')
validation_dir = os.path.join(base_dir, 'validation')

# Directory with our training cat pictures
train_cats_dir = os.path.join(train_dir, 'open')

# Directory with our training dog pictures
train_dogs_dir = os.path.join(train_dir, 'close')

# Directory with our validation cat pictures
validation_cats_dir = os.path.join(validation_dir, 'open')

# Directory with our validation dog pictures
validation_dogs_dir = os.path.join(validation_dir, 'close')

# The following code will let us check if the images have been loaded correctly:
# -------------------------------------------------------------------------------
# Set up matplotlib fig, and size it to fit 4x4 pics
import matplotlib.image as mpimg

nrows = 4
ncols = 4

fig = plt.gcf()
fig.set_size_inches(ncols * 4, nrows * 4)
pic_index = 100
train_cat_fnames = os.listdir(train_cats_dir)
train_dog_fnames = os.listdir(train_dogs_dir)

next_cat_pix = [os.path.join(train_cats_dir, fname)
                for fname in train_cat_fnames[pic_index - 8:pic_index]
                ]

next_dog_pix = [os.path.join(train_dogs_dir, fname)
                for fname in train_dog_fnames[pic_index - 8:pic_index]
                ]

for i, img_path in enumerate(next_cat_pix + next_dog_pix):
    # Set up subplot; subplot indices start at 1
    sp = plt.subplot(nrows, ncols, i + 1)
    sp.axis('Off')  # Don't show axes (or gridlines)

    img = mpimg.imread(img_path)
    plt.imshow(img)

plt.show()

# Pre-Trained Models for Image Classification
# ----------------------------------------------------------------------------------------------------------------------

# 1. Very Deep Convolutional Networks for Large-Scale Image Recognition(VGG-16)
# ------------------------------------------------------------------------------

# Step 1: Image Augmentation
# ---------------------------
# Add our data-augmentation parameters to ImageDataGenerator
train_datagen = ImageDataGenerator(rescale=1. / 255., rotation_range=40, width_shift_range=0.2, height_shift_range=0.2,
                                   shear_range=0.2, zoom_range=0.2, horizontal_flip=True)

# Note that the validation data should not be augmented!
test_datagen = ImageDataGenerator(rescale=1.0 / 255.)

# Step 2: Training and Validation Sets
# -------------------------------------
# Flow training images in batches of 20 using train_datagen generator
train_generator = train_datagen.flow_from_directory(train_dir, batch_size=20, class_mode='binary',
                                                    target_size=(224, 224))

# Flow validation images in batches of 20 using test_datagen generator
validation_generator = test_datagen.flow_from_directory(validation_dir, batch_size=20, class_mode='binary',
                                                        target_size=(224, 224))

# Step 3: Loading the Base Model
# -------------------------------
from tensorflow.keras.applications.vgg16 import VGG16

base_model = VGG16(input_shape=(224, 224, 3),  # Shape of our images
                   include_top=False,  # Leave out the last fully connected layer
                   weights='imagenet')

for layer in base_model.layers:
    layer.trainable = False

# Step 4: Compile and Fit
# ------------------------
# Flatten the output layer to 1 dimension
x = layers.Flatten()(base_model.output)

# Add a fully connected layer with 512 hidden units and ReLU activation
x = layers.Dense(512, activation='relu')(x)

# Add a dropout rate of 0.5
x = layers.Dropout(0.5)(x)

# Add a final sigmoid layer for classification
x = layers.Dense(1, activation='sigmoid')(x)

model = tf.keras.models.Model(base_model.input, x)

model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=0.0001), loss='binary_crossentropy', metrics=['acc'])

# Uncomment for VGG
# vgghist = model.fit(train_generator, validation_data=validation_generator, steps_per_epoch=100, epochs=7)
# ----------------------------------------------------------------------------------------------------------------------

# 2. Inception
# ------------------------------------------------------------------------------

# Step 1: Data Augmentation

# Add our data-augmentation parameters to ImageDataGenerator
train_datagen = ImageDataGenerator(rescale=1. / 255., rotation_range=40, width_shift_range=0.2, height_shift_range=0.2,
                                   shear_range=0.2, zoom_range=0.2, horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1.0 / 255.)

# Step 2: Training and Validation Generators
train_generator = train_datagen.flow_from_directory(train_dir, batch_size=20, class_mode='binary',
                                                    target_size=(150, 150))
validation_generator = test_datagen.flow_from_directory(validation_dir, batch_size=20, class_mode='binary',
                                                        target_size=(150, 150))

# Step 3: Loading the Base Model
from tensorflow.keras.applications.inception_v3 import InceptionV3

base_model = InceptionV3(input_shape=(150, 150, 3), include_top=False, weights='imagenet')

# Step 4: Compile and Fit
for layer in base_model.layers:
    layer.trainable = False

# We perform the following operations:
# - Flatten the output of our base model to 1 dimension
# - Add a fully connected layer with 1,024 hidden units and ReLU activation
# - This time, we will go with a dropout rate of 0.2
# - Add a final Fully Connected Sigmoid Layer
# - We will again use RMSProp, though you can try out the Adam Optimiser too
from tensorflow.keras.optimizers import RMSprop

x = layers.Flatten()(base_model.output)
x = layers.Dense(1024, activation='relu')(x)
x = layers.Dropout(0.2)(x)
x = layers.Dense(1, activation='sigmoid')(x)

model = tf.keras.models.Model(base_model.input, x)

model.compile(optimizer=RMSprop(lr=0.0001), loss='binary_crossentropy', metrics=['acc'])

# Uncomment for Inception
# inc_history = model.fit_generator(train_generator, validation_data=validation_generator, steps_per_epoch=100, epochs=7)
# ----------------------------------------------------------------------------------------------------------------------



