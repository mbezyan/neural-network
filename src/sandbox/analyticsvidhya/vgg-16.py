from main import *

# 1. Very Deep Convolutional Networks for Large-Scale Image Recognition(VGG-16)
# ------------------------------------------------------------------------------

# Step 1: Image Augmentation
# ---------------------------
# Add our data-augmentation parameters to ImageDataGenerator
train_datagen = ImageDataGenerator(rescale=1. / 255., rotation_range=40, width_shift_range=0.2, height_shift_range=0.2,
                                   shear_range=0.2, zoom_range=0.2, horizontal_flip=True)

# Note that the validation data should not be augmented!
test_datagen = ImageDataGenerator(rescale=1.0 / 255.)

# Step 2: Training and Validation Sets
# -------------------------------------
# Flow training images in batches of 20 using train_datagen generator
train_generator = train_datagen.flow_from_directory(training_dir, batch_size=32, class_mode='categorical',
                                                    target_size=(224, 224))

# Flow validation images in batches of 20 using test_datagen generator
validation_generator = test_datagen.flow_from_directory(validation_dir, batch_size=32, class_mode='categorical',
                                                        target_size=(224, 224))

# Step 3: Loading the Base Model
# -------------------------------
from tensorflow.keras.applications.vgg16 import VGG16

base_model = VGG16(input_shape=(224, 224, 3),  # Shape of our images
                   include_top=False,  # Leave out the last fully connected layer
                   weights='imagenet')

for layer in base_model.layers:
    layer.trainable = False

# Step 4: Compile and Fit
# ------------------------
# Flatten the output layer to 1 dimension
x = layers.Flatten()(base_model.output)

# Add a fully connected layer with 512 hidden units and ReLU activation
x = layers.Dense(1024, activation='relu')(x)

# Add a dropout rate of 0.5
x = layers.Dropout(0.2)(x)

# Add a final sigmoid layer for classification
x = layers.Dense(4, activation='softmax')(x)

model = tf.keras.models.Model(base_model.input, x)

model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=0.0001), loss='categorical_crossentropy', metrics=['acc'])

vgghist = model.fit(train_generator, validation_data=validation_generator, steps_per_epoch=160, epochs=6)