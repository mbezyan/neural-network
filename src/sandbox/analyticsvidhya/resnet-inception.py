from main import *

from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM, BatchNormalization, Flatten, GlobalAveragePooling2D
from keras.callbacks import TensorBoard
from keras.callbacks import ModelCheckpoint
# from keras.optimizers import adam

# 5. Inception-ResNet
# ------------------------------------------------------------------------------

# Step 1: Data Augmentation and Generators
# Add our data-augmentation parameters to ImageDataGenerator

train_datagen = ImageDataGenerator(rescale=1. / 255., rotation_range=40, width_shift_range=0.2, height_shift_range=0.2,
                                   shear_range=0.2, zoom_range=0.2, horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1.0 / 255.)

train_generator = train_datagen.flow_from_directory(train_dir, batch_size=5, class_mode='binary',
                                                    target_size=(224, 224))

validation_generator = test_datagen.flow_from_directory(validation_dir, batch_size=20, class_mode='binary',
                                                        target_size=(224, 224))

# Step 2: Import the base model
from tensorflow.keras.applications import InceptionResNetV2

base_model = InceptionResNetV2(input_shape=(224, 224, 3), include_top=False, weights="imagenet")

# Again, we are using only the basic ResNet model, so we will keep the layers frozen and only modify the last layer:
for layer in base_model.layers:
    layer.trainable = False

# Step 3: Build and Compile the Model
# Here, I would like to show you an even shorter code for using the ResNet50 model. We will use this model just as a
# layer in a Sequential model, and just add a single Fully Connected Layer on top of it.
from tensorflow.keras.applications import ResNet152V2
# from tensorflow.python.keras.models import Sequential
# from tensorflow.python.keras.layers import Dense, Flatten, GlobalAveragePooling2D

base_model = Sequential()
base_model.add(InceptionResNetV2(include_top=False, weights='imagenet', pooling='max'))
base_model.add(Dense(1, activation='sigmoid'))

# We compile the model and this time let us try the SGD optimizer:
base_model.compile(optimizer=tf.keras.optimizers.SGD(lr=0.0001), loss='binary_crossentropy', metrics=['acc'])

# Step 4: Fitting the model
resnet_history = base_model.fit(train_generator, validation_data=validation_generator, steps_per_epoch=100, epochs=15)
# ----------------------------------------------------------------------------------------------------------------------
