# Import
# ----------------------------------------------------------------------------------------------------------------------
import zipfile
import matplotlib.pyplot as plt
from src.constants import *

print("Started execution")

# Preparing the Dataset
# ----------------------------------------------------------------------------------------------------------------------
relative_training_validation_input_dir = '../' + TRAINING_VALIDATION_INPUT_DIR
input_image_package_name = '2-classes-grayscale_20210822-1636'

local_zip = relative_training_validation_input_dir + input_image_package_name + '.zip'
zip_ref = zipfile.ZipFile(local_zip, 'r')
print("Starting extraction: " + input_image_package_name)
zip_ref.extractall(relative_training_validation_input_dir)
zip_ref.close()
print("Completed extraction: " + input_image_package_name)

base_dir = relative_training_validation_input_dir + input_image_package_name
training_dir = os.path.join(base_dir, 'training')
validation_dir = os.path.join(base_dir, 'validation')

# classes = ['AAA', 'AAC', 'AAO', 'ACA', 'ACC', 'AOO', 'CAA', 'CAO', 'CCA', 'CCC', 'CCO', 'COO', 'OAA', 'OAC', 'OCA',
#            'OCC', 'OOA', 'OOC', 'OOO']
# classes = ['OPEN', 'CLOSED', 'ALMOST-CLOSED']
classes = ['OPEN', 'CLOSED']
number_of_classes = len(classes)

# Directories with our training pictures
training_dirs = []
for classification in classes:
    training_dirs.append(os.path.join(training_dir, classification))

# Directories with our validation pictures
validation_dirs = []
for classification in classes:
    validation_dirs.append(os.path.join(validation_dir, classification))

# The following code will let us check if the images have been loaded correctly:
# -------------------------------------------------------------------------------
# Set up matplotlib fig, and size it to fit 4x4 pics

nrows = 4
ncols = 2 * number_of_classes

fig = plt.gcf()
fig.set_size_inches(ncols * 4, nrows * 4)
pic_index = 100

training_fnames = []
for i in range(len(training_dirs)):
    training_fnames.append(os.listdir(training_dirs[i]))

next_pix = []
for i in range(len(training_dirs)):
    next_pix.append([os.path.join(training_dirs[i], fname)
                    for fname in training_fnames[i][pic_index - 8:pic_index]
                    ])

# for i, img_path in enumerate(next_open_pix + next_closing_pix + next_closed_pix + next_opening_pix):
# for i, img_path in enumerate([next_pic for next_pic in next_pix]):
#     # Set up subplot; subplot indices start at 1
#     sp = plt.subplot(nrows, ncols, i + 1)
#     sp.axis('Off')  # Don't show axes (or gridlines)
#
#     img = mpimg.imread(img_path)
#     plt.imshow(img)
#
# plt.show()
