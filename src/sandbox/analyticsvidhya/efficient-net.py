from main import *

# 4. EfficientNet
# ------------------------------------------------------------------------------

# import efficientnet.keras as efn
from tensorflow.keras.applications import EfficientNetB7

# Step 1: Image Augmentation
# Add our data-augmentation parameters to ImageDataGenerator

train_datagen = ImageDataGenerator(rescale=1. / 255., rotation_range=40, width_shift_range=0.2, height_shift_range=0.2,
                                   shear_range=0.2, zoom_range=0.2, horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1.0 / 255.)

train_generator = train_datagen.flow_from_directory(train_dir, batch_size=20, class_mode='binary',
                                                    target_size=(224, 224))

validation_generator = test_datagen.flow_from_directory(validation_dir, batch_size=20, class_mode='binary',
                                                        target_size=(224, 224))

# Step 2: Loading the Base Model
# base_model = efn.EfficientNetB0(input_shape=(224, 224, 3), include_top=False, weights='imagenet')
base_model = EfficientNetB7(input_shape=(224, 224, 3), include_top=False, weights='imagenet')
x = layers.Flatten()(base_model.output)
model = tf.keras.models.Model(base_model.input, x)

# Freeze the layers:
for layer in base_model.layers:
    layer.trainable = False

# Step 3: Build the model
x = model.output
x = layers.Flatten()(x)
x = layers.Dense(1024, activation="relu")(x)
x = layers.Dropout(0.5)(x)
predictions = layers.Dense(1, activation="sigmoid")(x)
model_final = tf.keras.Model(model.input, predictions)

# Step 4: Compile and Fit
from tensorflow.keras.optimizers import RMSprop
# model_final.compile(optimizers.rmsprop(lr=0.0001, decay=1e-6), loss='binary_crossentropy', metrics=['accuracy'])
model_final.compile(RMSprop(lr=0.0001, decay=1e-6), loss='binary_crossentropy', metrics=['accuracy'])

eff_history = model_final.fit_generator(train_generator, validation_data=validation_generator, steps_per_epoch=100,
                                        epochs=15)
