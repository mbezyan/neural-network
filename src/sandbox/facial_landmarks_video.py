import cv2
import mediapipe as mp
import numpy as np
import src.common.util.mp_util as mp_util
import src.common.mp_constants as mp_constants
import keyboard
import csv
import src.common.util.utils as utils

# Face Mesh
mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh()

# Webcam
#cap = cv2.VideoCapture(0)

# Video
video_name = "Video 32"
video_extension = "mp4"
# cap = cv2.VideoCapture("videos/" + video_name + "." + video_extension)

cap = utils.video_capture_handle_for_name(video_name)

# Output to video
# Read the first frame to capture
ret, image = cap.read()
height, width, _ = image.shape
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out_black = cv2.VideoWriter(video_name + '-landmarks-black.' + video_extension, fourcc, 30.0, (width, height))
out_video = cv2.VideoWriter(video_name + '-landmarks-video.' + video_extension, fourcc, 30.0, (width, height))

eyes_state = {}
LE_AREA_INDEX = 0
RE_AREA_INDEX = 1
LE_BLINKING_INDEX = 2
RE_BLINKING_INDEX = 3
previous_LE_area = 10000
consecutive_area_decrease_in_LE = 0
max_LE_area = None
min_LE_area = None
previous_RE_area = 10000
consecutive_area_decrease_in_RE = 0
max_RE_area = None
min_RE_area = None

last_LE_blinking_frame = None
last_RE_blinking_frame = None

delay_between_each_frame_ms = 0

frame = 1
# For each frame
while True:
    # Image
    ret, image = cap.read()

    # When ret is False, it means there are no more frames
    if ret is False:
        break
    height, width, _ = image.shape

    # Black frame
    black_frame = np.zeros((height, width, 3), np.uint8)

    #print("Height, Width:", height, width)
    rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    result = face_mesh.process(image)

    # Facial landmarks
    result = face_mesh.process(rgb_image)
    # For each face in the frame
    for facial_landmarks in result.multi_face_landmarks:
        i = 0
        landmark_coordinates = [None] * mp_constants.NUMBER_OF_LANDMARKS
        # For each landmark in the face
        for landmark in facial_landmarks.landmark:
            pt1 = landmark
            x = int(pt1.x * width)
            y = int(pt1.y * height)
            if i == mp_constants.BETWEEN_EYEBROWS_INDEX:
                between_eyebrows_x = x
                between_eyebrows_y = y
            if i == mp_constants.UNDER_LE_INDEX:
                under_LE_x = x
                under_LE_y = y
            if i == mp_constants.UNDER_RE_INDEX:
                under_RE_x = x
                under_RE_y = y

            # Add landmark index to frame displayed
            #cv2.putText(image, str(i), (x, y), 0, 0.3, (100, 100, 0))

            if i in mp_constants.EYES_LANDMARKS_INDICES:
                #print("x, y:", x, y)
                #cv2.circle(black_frame, (x, y), 1, (100, 255, 255), -1)
                cv2.circle(image, (x, y), 1, (100, 255, 255), -1)
                #cv2.putText(black_frame, str(i), (x, y), 0, 0.2, (100, 100, 0))
            landmark_coordinates[i] = (x, y)
            i += 1
        LE_area = mp_util.approximate_facial_landmark_polygon_area(mp_constants.LE_POLYGON_INDICES,
                                                                         landmark_coordinates)
        RE_area = mp_util.approximate_facial_landmark_polygon_area(mp_constants.RE_POLYGON_INDICES,
                                                                          landmark_coordinates)
        if frame % 100 == 0:
            print(frame)
            #print(eyes_state)

        eyes_state[frame] = [LE_area, RE_area, False, False]

        if (LE_area - previous_LE_area)/previous_LE_area < -0.05:
            consecutive_area_decrease_in_LE += 1
        elif (LE_area - previous_LE_area)/previous_LE_area > 0.05:
            consecutive_area_decrease_in_LE = 0

        if (RE_area - previous_RE_area)/previous_RE_area < -0.05:
            consecutive_area_decrease_in_RE += 1
        elif (RE_area - previous_RE_area)/previous_RE_area > 0.05:
            consecutive_area_decrease_in_RE = 0

        previous_LE_area = LE_area
        previous_RE_area = RE_area

        if consecutive_area_decrease_in_LE >= 2:
            cv2.circle(image, (under_LE_x, under_LE_y), 10, (100, 100, 100), -1)
            #consecutive_area_decrease_in_LE = 0
            eyes_state[frame][2] = True
            last_LE_blinking_frame = frame

        if consecutive_area_decrease_in_RE >= 2:
            cv2.circle(image, (under_RE_x, under_RE_y), 10, (100, 100, 100), -1)
            #consecutive_area_decrease_in_RE = 0
            eyes_state[frame][3] = True
            last_RE_blinking_frame = frame

    if keyboard.is_pressed('0'):
        cv2.circle(image, (200, 200), 10, (0, 0, 255), -1)
        eyes_state[last_LE_blinking_frame][2] = False
        eyes_state[last_RE_blinking_frame][3] = False

    if keyboard.is_pressed(' '):
        cv2.circle(image, (200, 200), 10, (0, 255, 0), -1)
        eyes_state[frame - 10][2] = True
        eyes_state[frame - 10][3] = True

    # Show on image
    cv2.imshow("Image", image)
    # Show on black background
    #cv2.imshow("black frame", black_frame)

    # Add frame to output video
    #out_video.write(image)
    #out_black.write(black_frame)

    cv2.waitKey(delay_between_each_frame_ms)
    frame += 1

    # if frame > 30:
    #     break

# Release output video handle
#out_black.release()
#out_video.release()

# Post processing
# Find minimums and maximums
min_LE_area = eyes_state[1][LE_AREA_INDEX]
max_LE_area = eyes_state[1][LE_AREA_INDEX]
min_RE_area = eyes_state[1][RE_AREA_INDEX]
max_RE_area = eyes_state[1][RE_AREA_INDEX]
for key in eyes_state:
    if eyes_state[key][LE_AREA_INDEX] < min_LE_area:
        min_LE_area = eyes_state[key][LE_AREA_INDEX]
    elif eyes_state[key][LE_AREA_INDEX] > max_LE_area:
        max_LE_area = eyes_state[key][LE_AREA_INDEX]
    if eyes_state[key][RE_AREA_INDEX] < min_RE_area:
        min_RE_area = eyes_state[key][RE_AREA_INDEX]
    elif eyes_state[key][RE_AREA_INDEX] > max_RE_area:
        max_RE_area = eyes_state[key][RE_AREA_INDEX]

# Write to CSV file
csv_column_headers = ['Frame', 'LE area', 'RE area', 'LE blinking', 'RE blinking']
csv_file_name = video_name + '.csv'
with open(csv_file_name, 'w', newline='', encoding='utf-8') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(csv_column_headers)
    for key, value in eyes_state.items():
        eyes_state[key][LE_AREA_INDEX] = (eyes_state[key][LE_AREA_INDEX] - min_LE_area) / max_LE_area
        eyes_state[key][RE_AREA_INDEX] = (eyes_state[key][RE_AREA_INDEX] - min_RE_area) / max_RE_area
        eyes_state[key][LE_BLINKING_INDEX] = int(eyes_state[key][LE_BLINKING_INDEX])
        eyes_state[key][RE_BLINKING_INDEX] = int(eyes_state[key][RE_BLINKING_INDEX])
        writer.writerow([key] + value)
