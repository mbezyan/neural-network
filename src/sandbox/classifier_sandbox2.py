# import the necessary packages
from tensorflow.keras.models import load_model
import numpy as np
import os
import imutils
import cv2


CLASSES = ['closed', 'closing', 'open', 'opening']
MODEL_PATH = os.path.sep.join(["output", "food11.model"])
# load the trained model from disk
model = load_model(MODEL_PATH)

classes4 = {'closed': [0, 0],
            'closing': [1, 0],
            'open': [2, 0],
            'opening': [3, 0]}

videos = [
    # '01-asian-female-20210904-150928',
    # '02-australian-white-male1-M0gGLCXR1n0-20210904-151132',
    # '03-black-female1-k2jiK8zWsb0-20210904-151220',
    # '05-black-male-news-reader-XMrxsEDtyTQ-20210904-151533',
    # '06-blond-white-male-uwPtFbA1sRY-20210904-152041',
    # '07-caucasian-male1-youtube-_hjfQ41LF0w-20210904-153133',
    # '08-caucasian-male-news-reader-wZNng505VYM-20210904-153326',
    # '09-eastern-european-female-youtube-Tjom_yAUj-s-20210904-152147',
    # '10-indian-female1-ZD6jw2FvWaE-20210904-152357',
    # '11-indian-male1-c9F5kMUfFKk-20210904-153541',
    # '12-man-glasses-monitor-reflection-20210904-153809',
    # '13-mohammad-reading-20210904-152609',
    # '14-white-female-news-interview-XMrxsEDtyTQ-20210904-152817',
    # '15-white-male-bald-round-face-uwPtFbA1sRY-20210904-152948',
    # '16-african-canadian-french-female-glasses-mGWzHsgld58-20210914-164517',
    # '17-cacusian-male-EACDJJW3aqo-20210916-221707',
    # '18-australia-white-male-glasses-Y6QtKgB8K3Q-20210917-211109'
    '19-indian-male-5j7KxVTNBdY-20210918-224431'
]
for video in videos:
    classfied_imgs_dir = '../../out/images/4-classes/' + video + '/classified'
    for classification in classes4.keys():
        print(classification)
        classification_dir = os.path.join(classfied_imgs_dir, classification)
        print(classification_dir)
        image_count = 0
        for image in os.listdir(classification_dir):
            image_count += 1
            imagePath = os.path.join(classification_dir, image)
            image = cv2.imread(imagePath)
            output = image.copy()
            output = imutils.resize(output, width=400)

            # our model was trained on RGB ordered images but OpenCV represents
            # images in BGR order, so swap the channels, and then resize to
            # 224x224 (the input dimensions for VGG16)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = cv2.resize(image, (224, 224))

            # convert the image to a floating point data type and perform mean
            # subtraction
            image = image.astype("float32")
            mean = np.array([123.68, 116.779, 103.939][::1], dtype="float32")
            image -= mean

            # pass the image through the network to obtain our predictions
            preds = model.predict(np.expand_dims(image, axis=0))[0]
            i = np.argmax(preds)
            label = CLASSES[i]
            classes4[label][1] += 1
            # text = "{}: {:.2f}%".format(label, preds[i] * 100)
            # print(imagePath)
            # print(text)

        for key, value in classes4.items():
            print(key, ":", value[1]/image_count)

        for key, value in classes4.items():
            classes4[key][1] = 0
