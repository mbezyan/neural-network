import time


def function2():
    time.sleep(2)


def function1():
    time.sleep(1)
    function2()


for i in range(0, 10):
    function1()
