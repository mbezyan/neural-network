import cv2
import mediapipe as mp

# Face Mesh
import mp_constants
import mp_util

mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh()



# Image
image = cv2.imread("images/open-eyes-cropped.jpg")
height, width, _ = image.shape
print("Height, Width:", height, width)
rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Scale up
scale_percent = 410 # percent of original size
width = int(width * scale_percent / 100)
height = int(height * scale_percent / 100)
dim = (width, height)
rbg_image = cv2.resize(rgb_image, dim)

# Facial landmarks
result = face_mesh.process(rgb_image)
for facial_landmarks in result.multi_face_landmarks:
    i = 0
    landmark_coordinates = [None] * mp_constants.NUMBER_OF_LANDMARKS
    for landmark in facial_landmarks.landmark:
        pt1 = landmark
        x = int(pt1.x * width)
        y = int(pt1.y * height)
        print(str(i) + ":", x, y)
        # cv2.circle(image, (x, y), 1, (100, 100, 0), -1)
        if i in mp_constants.EYES_LANDMARKS_INDICES:
            cv2.putText(rbg_image, str(i), (x, y), 0, 0.4, (255, 255, 255))
        landmark_coordinates[i] = (x, y)
        i += 1
    left_eye_area = mp_util.approximate_facial_landmark_polygon_area(mp_constants.LE_POLYGON_INDICES,
                                                                     landmark_coordinates)
    right_eye_area = mp_util.approximate_facial_landmark_polygon_area(mp_constants.RE_POLYGON_INDICES,
                                                                      landmark_coordinates)
    print(left_eye_area)
    print(right_eye_area)

# Show image and wait for user input before closing the window
cv2.imshow("Image", rbg_image)
cv2.waitKey(0)
