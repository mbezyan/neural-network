import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import time
import threading

start = -1000
finish = 1000

print("Prep")
fig, ax = plt.subplots()
xdata, ydata = [], []
ln, = plt.plot([], [], 'ro')
plotted_frame = 0


def init():
    print("Init called")
    ax.set_xlim(start, finish)
    ax.set_ylim(0, finish*finish)
    return ln,


def update(x):
    global plotted_frame
    print("Update called: " + str(plotted_frame))
    if plotted_frame < len(calculated_y) - 1:
        plotted_frame += 1
        xdata.append(calculated_y[int(plotted_frame)][0])
        ydata.append(calculated_y[int(plotted_frame)][1])
        ln.set_data(xdata, ydata)
    return ln,


def calculate_y():
    for i in range(start, finish+1):
        print("In for loop: " + str(i))
        calculated_y.append((i, i * i))
        # print(calculated_y)
        time.sleep(0.001)


calculated_y = []
ani = FuncAnimation(fig,
                    update,
                    # frames=(x for x in range(0, 1000*2)),
                    # frames=np.linspace(0, finish*2, finish*2),
                    init_func=init,
                    interval=1,
                    blit=True,
                    repeat=False)

print("calculated_y init")

thread1 = threading.Thread(target=calculate_y)
thread1.start()
print("started thread 1")
# thread2.start()
print("started thread 2")
thread3 = threading.Thread(target=plt.show())
# thread3.start()
print("started thread 3")
