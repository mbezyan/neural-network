from os import listdir
import constants
import common.util.utils as util
from common.eye import Eye
from src.common.eye_state import *
from src.common.custom_exceptions import *


def frame_state(frame, closed_frames, open_frames):
    if frame in closed_frames:
        return EyeStateCO.CLOSED
    elif frame in open_frames:
        return EyeStateCO.OPEN
    else:
        raise UnclassifiedEyeFrame(frame)


def running_frame_state(frame, closed_frames, open_frames):
    c = closed_frames
    o = open_frames
    f1 = frame - 2
    f2 = frame - 1
    f3 = frame
    f1_state = frame_state(f1, closed_frames, open_frames)
    f2_state = frame_state(f2, closed_frames, open_frames)
    f3_state = frame_state(f3, closed_frames, open_frames)
    return stationary_to_moving_state([f1_state, f2_state, f3_state])


def extract_classify_store_eyes_from_video(video_name):
    logger.info("Starting processing: " + video_name)

    # Create directory for output
    file_dir_name = video_name + '-' + util.timestamp()

    # Create directory for images
    imgs_dir_name = os.path.join(constants.IMAGES_OUTPUT_DIR, file_dir_name)
    classified_imgs_dir_name = os.path.join(imgs_dir_name, 'classified')
    os.mkdir(imgs_dir_name)
    os.mkdir(classified_imgs_dir_name)
    for state in EyeStateCCOO:
        os.mkdir(os.path.join(classified_imgs_dir_name, state.name))

    # Retrieve single-frame image classifications
    classified_imgs_dir = video_name + '-' + constants.videos[video_name]['classified_imgs_dir']
    classified_imgs_dir_path = os.path.join(constants.IMAGES_OUTPUT_DIR, '2-classes-co', classified_imgs_dir, 'classified')
    open_imgs = listdir(os.path.join(classified_imgs_dir_path, 'open'))
    closed_imgs = listdir(os.path.join(classified_imgs_dir_path, 'closed'))
    right_open = [int(file_name.split('-')[2].split('.')[0]) for file_name in open_imgs if '-right-' in file_name]
    right_closed = [int(file_name.split('-')[2].split('.')[0]) for file_name in closed_imgs if
                    '-right-' in file_name]
    left_open = [int(file_name.split('-')[2].split('.')[0]) for file_name in open_imgs if '-left-' in file_name]
    left_closed = [int(file_name.split('-')[2].split('.')[0]) for file_name in closed_imgs if '-left-' in file_name]

    video_capture_handle = util.video_capture_handle_for_name(video_name)
    img_name_prefix = constants.videos[video_name]['img_name_prefix']

    LE_running_frame = [None] * constants.number_of_imgs_to_stack
    RE_running_frame = [None] * constants.number_of_imgs_to_stack
    frame = 0
    frames_processed = 0
    max_frames_classified = max(max(right_open), max(right_closed), max(left_open), max(left_closed))
    logger.info("Max frames: " + str(max_frames_classified))
    # For each frame until the end of the video or after 1 minute of the video has been processed
    while True and (frame <= max_frames_classified):
        frame += 1

        # Image
        ret, image = video_capture_handle.read()

        # When ret is False, it means there are no more frames
        if ret is False:
            logger.info("Frame " + str(frame-1) + " was the last frame.")
            break

        try:
            left_eye, right_eye = util.extract_LEEB_REEB_from_image(image)[0:2]
        except NoFacialLandmarksFound:
            # Go to next frame and skip this one
            logger.warning("Frame " + str(frame) + " skipped because no landmarks were found")
            continue
        except LeftEyeEyebrowNotFound:
            logger.warning("Frame " + str(frame) + " skipped because left eye was not detected")
            continue
        except RightEyeEyebrowNotFound:
            logger.warning("Frame " + str(frame) + " skipped because right eye was not detected")
            continue

        # Keep two previous frames in memory
        util.keep_previous_frames_in_memory(LE_running_frame, RE_running_frame, left_eye, right_eye)

        # From frame 3 onwards start merging with the two previous frame
        if frames_processed >= constants.number_of_imgs_to_stack:
            right_eye_merged = util.three_grayscale_imgs_to_one_rgb(RE_running_frame)
            left_eye_merged = util.three_grayscale_imgs_to_one_rgb(LE_running_frame)

            try:
                RE_classification = util.running_frame_state(frame, right_closed, right_open)
                util.save_eye_image(right_eye_merged, frame, os.path.join(classified_imgs_dir_name, RE_classification.name),
                                    img_name_prefix + "-right-")
            except UnclassifiedEyeFrame as error:
                logger.error(str(Eye.RIGHT.value) + ": " + str(error.frame) + " does not exist in classified images.")
                continue

            try:
                LE_classification = util.running_frame_state(frame, left_closed, left_open)
                util.save_eye_image(left_eye_merged, frame, os.path.join(classified_imgs_dir_name, LE_classification.name),
                                    img_name_prefix + "-left-")
            except UnclassifiedEyeFrame as error:
                logger.error(str(Eye.LEFT.value) + ": " + str(error.frame) + " does not exist in classified images.")
                continue

        frames_processed += 1

        if frame % 300 == 0:
            logger.info(str(frame))

    logger.info("Completed processing: " + video_name)


def extract_classify_store_eyes_from_videos(video_names):
    for video_name in video_names:
        extract_classify_store_eyes_from_video(video_name)


# Call function
extract_classify_store_eyes_from_videos([
    # '01-asian-female',
    # '02-australian-white-male1-M0gGLCXR1n0',
    # '03-black-female1-k2jiK8zWsb0',
    # '04-black-female2-SskrmHCoiRc',
    # '05-black-male-news-reader-XMrxsEDtyTQ',
    # '06-blond-white-male-uwPtFbA1sRY',
    # '07-caucasian-male1-youtube-_hjfQ41LF0w',
    # '08-caucasian-male-news-reader-wZNng505VYM',
    # '09-eastern-european-female-youtube-Tjom_yAUj-s',
    # '10-indian-female1-ZD6jw2FvWaE',
    # '11-indian-male1-c9F5kMUfFKk',
    # '12-man-glasses-monitor-reflection',
    # '13-mohammad-reading',
    # '14-white-female-news-interview-XMrxsEDtyTQ',
    # '15-white-male-bald-round-face-uwPtFbA1sRY',
    # '16-african-canadian-french-female-glasses-mGWzHsgld58',
    # '17-cacusian-male-EACDJJW3aqo',
    # '18-australia-white-male-glasses-Y6QtKgB8K3Q',
    # '19-indian-male-5j7KxVTNBdY',
    # '20-white-obese-male1-TQHW7gGjrCQ',
    # '21-hispanic-female1-Qlu_7ZcbSW0',
    # '23-caucasian-american-female1-aIvRs-M8n8o',
    # '24-caucasian-american-female2-aIvRs-M8n8o',
    # '25-white-german-male1-7YHZ2qXXPxw',
    # '26-while-female-eye-makeup2-eE1kAY87UbM',
    # '27-caucasian-male2-XCjosBT3Gy8',
    # '28-caucasian-middle-aged-male-X2mVL_trjmo',
    # '29-caucasian-young-male-angle1-zgwMnvPbzuQ',
    # '30-asian-female2-kayOhGRcNt4',
    # '31-cacucasian-male-bald-glasses1-wIjK-6Do6lg',
    # '32-caucasian-young-male2-oocNVvTHP5M',
    # '33-indian-male-glasses1-DqNb0png0gA',
    # '34-caucasian-middle-aged-female-light-makeup1-oYFCbCZF8hA',
    '35-caucasian-old-american-male1-eI65CbmTlAc',
])
