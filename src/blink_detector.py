import sys
import src.common.util.utils as utils
import csv
import src.common.eye_state as eye_state
from src.common.eye import Eye
from collections import deque
from tensorflow.keras.preprocessing.image import load_img
from src.classfication_util import calc_blink_state_grayscale, calculate_blink_state_tensor, both_eyes_blinking
import cv2
from src import constants
from src.common.custom_exceptions import NoFacialLandmarksFound, LeftEyeEyebrowNotFound, RightEyeEyebrowNotFound
import timeit
from src.common.util.logging_util import logger
import os
import inspect
import tracemalloc

tracemalloc.start()

__start = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': Started @ ' + str(__start))

# classification_input_type = 'single-grayscale-frame'
classification_input_type = 'three-stacked-frames'
async_eye_blink_threshold = 3
show_video = True
delay_between_frames = 0
min_states_for_blink = 3
max_states_for_blink = 3  # 4
# If the eye remains in one state for a second (30 frames) or more then it's not blinking
max_frames_for_blink = 30
start_processing_frame = 1
frames_to_process = 3000
frame_blink_for_plotting = []


def annotate_image(image, frame, blink_count,
                   under_le_coords, under_re_coords,
                   le_classification_label, re_classification_label,
                   re_blinking, le_blinking, eyes_coords):
    cv2.putText(image, str(frame), (20, 30), 0, 1, (0, 0, 255), thickness=3)
    under_le_coords = (under_le_coords[0] + 20, under_le_coords[1] + 40)
    cv2.putText(image, le_classification_label, under_le_coords, 0, 1, (0, 255, 255), thickness=3)
    under_re_coords = (under_re_coords[0] - 20, under_re_coords[1] + 40)
    cv2.putText(image, re_classification_label, under_re_coords, 0, 1, (0, 255, 255), thickness=3)
    centre_coords = (under_re_coords[0] - 40, under_re_coords[1] + 60)
    lower_centre_coords = (under_re_coords[0] - 40, under_re_coords[1] + 100)
    cv2.putText(image, "Blinks: " + str(blink_count), lower_centre_coords, 0, 1, (0, 255, 255), thickness=3)
    cv2.putText(image, str(re_blinking) + " - " + str(le_blinking), centre_coords, 0, 1, (0, 255, 255), thickness=3)
    # [[lv1, lh1, lv2, lh2], [rv1, rh1, rv2, rh2]]
    cv2.rectangle(image, (eyes_coords[0][0], eyes_coords[0][1]), (eyes_coords[0][2], eyes_coords[0][3]), (0, 0, 255))
    cv2.rectangle(image, (eyes_coords[1][0], eyes_coords[1][1]), (eyes_coords[1][2], eyes_coords[1][3]), (0, 0, 255))


# class DetectBlinksInVideo(Thread):
#     def run(self):
def detect_blinks_in_video(video_name):
    start = timeit.default_timer()
    function_name = __file__ + '.' + inspect.stack()[0][3]
    logger().debug("[PROFILING] " + function_name + ': Started @ ' + str(__start))

    # video_name = '38-black-female3-VxulD6o1a5g'
    # video_name = '05-black-male-news-reader-XMrxsEDtyTQ'
    logger().info("Starting processing: " + video_name)

    video_capture_handle = utils.video_capture_handle_for_name(video_name)
    # showing values of the properties
    print("CV_CAP_PROP_FRAME_WIDTH: '{}'".format(video_capture_handle.get(cv2.CAP_PROP_FRAME_WIDTH)))
    print("CV_CAP_PROP_FRAME_HEIGHT : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    print("CAP_PROP_FPS : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_FPS)))
    print("CAP_PROP_POS_MSEC : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_POS_MSEC)))
    print("CAP_PROP_FRAME_COUNT  : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_FRAME_COUNT)))
    print("CAP_PROP_BRIGHTNESS : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_BRIGHTNESS)))
    print("CAP_PROP_CONTRAST : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_CONTRAST)))
    print("CAP_PROP_SATURATION : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_SATURATION)))
    print("CAP_PROP_HUE : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_HUE)))
    print("CAP_PROP_GAIN  : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_GAIN)))
    print("CAP_PROP_CONVERT_RGB : '{}'".format(video_capture_handle.get(cv2.CAP_PROP_CONVERT_RGB)))

    le_running_frame = [None] * constants.number_of_imgs_to_stack
    le_last_state_transition_frame = sys.maxsize
    le_classification = eye_state.EyeStateCCOO.OPEN.value  # eye_state.EyeStateCO.OPEN.value #
    le_classification_label = eye_state.EyeStateCCOO.OPEN.name  # eye_state.EyeStateCO.OPEN.value #
    le_new_states = deque([])
    le_blinking = False
    le_blinking_frame = None
    le_blinking_frames = deque([])
    le_blink_intensity = None

    re_running_frame = [None] * constants.number_of_imgs_to_stack
    re_last_state_transition_frame = sys.maxsize
    re_classification = eye_state.EyeStateCCOO.OPEN.value  # eye_state.EyeStateCO.OPEN.value #
    re_classification_label = eye_state.EyeStateCCOO.OPEN.name  # eye_state.EyeStateCO.OPEN.value #
    re_new_states = deque([])
    re_blinking = False
    re_blinking_frame = None
    re_blinking_frames = deque([])
    re_blink_intensity = None

    blinks = []
    blink_count = 0
    eye_states = []
    eye_states.append(["Frame",
                       "L-class", "L-blnk", "L-blnk-f",
                       "R-class", "R-blnk", "R-blnk-f"])

    # For each frame until the end of the video
    frame = 0
    frames_processed = 0
    while True and frame < frames_to_process:
        start1 = timeit.default_timer()
        component_name = function_name + ' - main loop'
        logger().debug("[PROFILING] " + component_name + ': Started @ ' + str(start1))

        frame += 1
        if frame % 500 == 0:
            logger().info("Video frame: " + str(frame))

        # Image
        ret, image = video_capture_handle.read()

        # When ret is False, it means there are no more frames
        if ret is False:
            logger().info("Reached end of video")
            break

        if frame < start_processing_frame:
            continue

        try:

            left_eye, right_eye, under_le_coords, under_re_coords, eyes_coords = utils.extract_LEEB_REEB_from_image(
                image)
        except NoFacialLandmarksFound:
            # Go to next frame and skip this one
            logger().error("Frame " + str(frame) + " skipped because no landmarks were found")
            continue
        except LeftEyeEyebrowNotFound:
            logger().error("Frame " + str(frame) + " skipped because left eye was not detected")
            continue
        except RightEyeEyebrowNotFound:
            logger().error("Frame " + str(frame) + " skipped because right eye was not detected")
            continue

        if classification_input_type == 'three-stacked-frames':
            # Keep two previous frames in memory
            utils.keep_previous_frames_in_memory(le_running_frame, re_running_frame, left_eye, right_eye)

            # From frame 3 onwards start merging with the two previous frame
            if frames_processed >= constants.number_of_imgs_to_stack:
                le_classification, le_classification_label, le_last_state_transition_frame, le_new_states, \
                    le_blinking, le_blinking_frames, le_blink_intensity = \
                    calculate_blink_state_tensor(Eye.LEFT, frame, le_running_frame, le_last_state_transition_frame,
                                                 le_classification, le_new_states, le_blinking_frames)

                re_classification, re_classification_label, re_last_state_transition_frame, re_new_states, \
                    re_blinking, re_blinking_frames, re_blink_intensity = \
                    calculate_blink_state_tensor(Eye.RIGHT, frame, re_running_frame, re_last_state_transition_frame,
                                                 re_classification, re_new_states, re_blinking_frames)

        elif classification_input_type == 'single-grayscale-frame':
            temp_timestamp = utils.timestamp()
            image_path = os.path.join(constants.TEMP_OUTPUT_DIR, temp_timestamp + '-left-' + str(frame) + '.png')
            cv2.imwrite(image_path, left_eye)
            left_eye_for_classification = load_img(image_path, target_size=(224, 224))
            image_path = os.path.join(constants.TEMP_OUTPUT_DIR, temp_timestamp + '-right-' + str(frame) + '.png')
            cv2.imwrite(image_path, right_eye)
            right_eye_for_classification = load_img(image_path, target_size=(224, 224))
            le_classification, le_classification_label, le_last_state_transition_frame, le_new_states, le_blinking = \
                calc_blink_state_grayscale(Eye.LEFT, frame, left_eye_for_classification,
                                           le_last_state_transition_frame,
                                           le_classification, le_new_states)
            re_classification, re_classification_label, re_last_state_transition_frame, re_new_states, re_blinking = \
                calc_blink_state_grayscale(Eye.RIGHT, frame, right_eye_for_classification,
                                           re_last_state_transition_frame,
                                           re_classification, re_new_states)

        eye_states.append(
            [frame, le_classification_label, le_blinking, None, re_classification_label, re_blinking, None])

        if le_blinking:
            le_blinking_frame = frame
        if re_blinking:
            re_blinking_frame = frame
        if both_eyes_blinking(le_blinking_frame, re_blinking_frame):
            logger().info("L+R: " + str(frame) + "[" + str(le_blinking_frame) + ", " + str(re_blinking_frame) + "]")
            blink_count += 1
            blinks.append([le_blinking_frame, re_blinking_frame, (le_blinking_frame + re_blinking_frame) / 2])
            eye_states[-1][3] = le_blinking_frame
            eye_states[-1][6] = re_blinking_frame
            le_blinking_frame = None
            re_blinking_frame = None
        #     frame_blink_for_plotting.append((frame, [1, image, right_eye, left_eye]))
        # else:
        #     frame_blink_for_plotting.append((frame, [0, image, right_eye, left_eye]))

        # print(eye_states[-1])

        if show_video:
            annotate_image(image, frame, blink_count,
                           under_le_coords, under_re_coords,
                           le_classification_label, re_classification_label,
                           re_blinking, le_blinking, eyes_coords)
            cv2.imshow("Image", image)
            cv2.imshow("Right eye", right_eye)
            cv2.imshow("Left eye", left_eye)
            cv2.waitKey(delay_between_frames)

        frames_processed += 1
        # time.sleep(0.030)
        stop1 = timeit.default_timer()
        logger().debug("[PROFILING] " + component_name + ': ُStopped @ ' + str(stop1))
        logger().debug("[PROFILING] " + component_name + ': ' + str(stop1 - start1))

    with open(os.path.join(constants.CSV_OUTPUT_DIR, video_name + '-' + utils.timestamp() + '.csv'), "w",
              newline="") as f:
        writer = csv.writer(f)
        writer.writerows(eye_states)

    stop = timeit.default_timer()
    logger().debug("[PROFILING] " + function_name + ': ُStopped @ ' + str(stop))
    logger().debug("[PROFILING] " + function_name + ': ' + str(stop - start))


snapshot1 = tracemalloc.take_snapshot()

# detect_blinks_in_video('16-african-canadian-french-female-glasses-mGWzHsgld58')
# detect_blinks_in_video('26122013_223310_cam')
# detect_blinks_in_video('26122013_230103_cam')
# detect_blinks_in_video('mohammad-iq-test-1-of-3')
#detect_blinks_in_video('19-indian-male-5j7KxVTNBdY')
# detect_blinks_in_video('talking')
# detect_blinks_in_video('02-australian-white-male1-M0gGLCXR1n0')
# detect_blinks_in_video('38-black-female3-VxulD6o1a5g')
# detect_blinks_in_video('37-white-female3-nuxzL6f2SCU')
detect_blinks_in_video('Video 32')

snapshot2 = tracemalloc.take_snapshot()

top_stats = snapshot2.compare_to(snapshot1, 'lineno')

print("[ Top 10 differences ]")
for stat in top_stats[:10]:
    print(stat)

__stop = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ُStopped @ ' + str(__stop))
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ' + str(__stop - __start))
