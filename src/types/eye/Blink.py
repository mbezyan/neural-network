from collections import deque


class Blink:
    def __init__(self, frames=None, start_ts=0, end_ts=0, intensity=0.0, blinking=False):
        self.frames = deque([]) if frames is None else frames
        self.start_ts = start_ts
        self.end_ts = end_ts
        self.intensity = intensity
        self.blinking = blinking
