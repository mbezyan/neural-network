from collections import deque
from src.types.eye.Blink import Blink
from sys import maxsize


class IsBlinkingIn:
    def __init__(self, eye, frame=0, clazz=None, previous_clazz=2, last_state_transition=maxsize, new_states=None, blink=None):
        self.frame = frame
        self.eye = eye
        self.clazz = clazz
        self.previous_clazz = previous_clazz
        self.last_state_transition = last_state_transition
        self.new_states = deque([]) if new_states is None else new_states
        self.blink = Blink() if blink is None else blink
