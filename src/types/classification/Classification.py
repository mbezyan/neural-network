class Classification:
    def __init__(self, clazz=-1, label="", probability=0.0):
        self.clazz = clazz
        self.label = label
        self.probability = probability
