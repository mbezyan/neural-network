import csv
import time

import numpy as np

from src import constants
import os
import cv2
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

# Retrieve eye states CSV into array
video = '38-black-female3-VxulD6o1a5g-20211215-191134'
data_dir = os.path.join(constants.VIDEOS_INPUT_DIR, video)


def create_graph_images(target):
    csv_file_path = os.path.join(data_dir, 'eye-states.csv')
    with open(csv_file_path, newline='') as csvfile:
        data = list(csv.reader(csvfile))

    axs = 4 * [None]
    fig = plt.figure(constrained_layout=False)
    fig.set_size_inches(15, 10)
    gs = GridSpec(3, 4, figure=fig)
    face = fig.add_subplot(gs[0:2, 0:3])
    left_eye = fig.add_subplot(gs[1, 3])
    right_eye = fig.add_subplot(gs[0, 3])

    graph = fig.add_subplot(gs[2, :])
    xdata, ydata = [], []
    ln, = graph.plot([], [])
    graph.set_xlim(0, len(data) + 1)
    graph.set_ylim(0, 1.5)
    graph.set_ylabel('Blink intensity')
    graph.set_xlabel('Frames @ 30 fps - wavelength signifies blink duration')

    face.axis('off')
    left_eye.axis('off')
    right_eye.axis('off')

    # For each frame retrieve the images
    for i in range(target, target+6):
        image_path = os.path.join(data_dir, data[i][0] + '-face.png')
        face_image = cv2.imread(image_path)
        face_image = cv2.cvtColor(face_image, cv2.COLOR_RGB2BGR)
        # cv2.imshow("Face", face_image)

        image_path = os.path.join(data_dir, data[i][0] + '-left.png')
        le_image = cv2.imread(image_path)
        # cv2.imshow("Left", le_image)

        image_path = os.path.join(data_dir, data[i][0] + '-right.png')
        re_image = cv2.imread(image_path)
        # cv2.imshow("Right", re_image)

        # cv2.waitKey(0)

        xdata.append(int(data[i][0]))
        ydata.append(float(data[i][8]))
        ln.set_data(xdata, ydata)
        # ax.relim()
        face.imshow(face_image)
        left_eye.imshow(le_image)
        right_eye.imshow(re_image)
        print(i)
        plt.savefig(os.path.join(data_dir, str(i) + '-fig'))
        # plt.pause(1)

    # plt.show()


def animate_graph():
    for i in range(1, 100):
        image_path = os.path.join(data_dir, str(i) + '-fig.png')
        face_image = cv2.imread(image_path)
        print(i)
        cv2.imshow("Graph", face_image)
        cv2.waitKey(0)


# for i in np.arange(115, 600, 5):
#     create_graph_images(i)
animate_graph()
