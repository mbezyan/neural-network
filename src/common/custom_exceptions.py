class NoFacialLandmarksFound(Exception):
    pass


class LeftEyeEyebrowNotFound(Exception):
    pass


class RightEyeEyebrowNotFound(Exception):
    pass


class FileNotFoundException(Exception):
    pass


class UnclassifiedEyeFrame(Exception):
    def __init__(self, frame):
        self.frame = frame
