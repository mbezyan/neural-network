from enum import Enum
import timeit
import os
from src.common.util.logging_util import logger

__start = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': Started @ ' + str(__start))


class EyeStateCCOO(Enum):
    CLOSED = 0
    CLOSING = 1
    OPEN = 2
    OPENING = 3


EYE_STATE_CCOO_FROM_STRING = {
    'closed': EyeStateCCOO.CLOSED,
    'closing': EyeStateCCOO.CLOSING,
    'open': EyeStateCCOO.OPEN,
    'opening': EyeStateCCOO.OPENING
}


class EyeStateACO(Enum):
    ALMOST_CLOSED = 0
    CLOSED = 1
    OPEN = 2


class EyeStateCO(Enum):
    CLOSED = 0
    OPEN = 1


blink_states = {
    str(EyeStateCCOO.OPEN.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.OPEN.value):
        (EyeStateCCOO.OPEN, EyeStateCCOO.CLOSING, EyeStateCCOO.OPEN),
    str(EyeStateCCOO.OPEN.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.OPENING.value):
        (EyeStateCCOO.OPEN, EyeStateCCOO.CLOSING, EyeStateCCOO.OPENING),
    str(EyeStateCCOO.OPEN.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPEN.value):
        (EyeStateCCOO.OPEN, EyeStateCCOO.CLOSING, EyeStateCCOO.CLOSED, EyeStateCCOO.OPEN),
    str(EyeStateCCOO.OPEN.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPENING.value):
        (EyeStateCCOO.OPEN, EyeStateCCOO.CLOSING, EyeStateCCOO.CLOSED, EyeStateCCOO.OPENING),
    str(EyeStateCCOO.OPEN.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPEN.value):
        (EyeStateCCOO.OPEN, EyeStateCCOO.CLOSED, EyeStateCCOO.OPEN),
    str(EyeStateCCOO.OPEN.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPENING.value):
        (EyeStateCCOO.OPEN, EyeStateCCOO.CLOSED, EyeStateCCOO.OPENING),
    str(EyeStateCCOO.OPENING.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.OPEN.value):
        (EyeStateCCOO.OPENING, EyeStateCCOO.CLOSING, EyeStateCCOO.OPEN),
    str(EyeStateCCOO.OPENING.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.OPENING.value):
        (EyeStateCCOO.OPENING, EyeStateCCOO.CLOSING, EyeStateCCOO.OPENING),
    str(EyeStateCCOO.OPENING.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPEN.value):
        (EyeStateCCOO.OPENING, EyeStateCCOO.CLOSING, EyeStateCCOO.CLOSED, EyeStateCCOO.OPEN),
    str(EyeStateCCOO.OPENING.value) + str(EyeStateCCOO.CLOSING.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPENING.value):
        (EyeStateCCOO.OPENING, EyeStateCCOO.CLOSING, EyeStateCCOO.CLOSED, EyeStateCCOO.OPENING),
    str(EyeStateCCOO.OPENING.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPENING.value):
        (EyeStateCCOO.OPENING, EyeStateCCOO.CLOSED, EyeStateCCOO.OPENING),
    str(EyeStateCCOO.OPENING.value) + str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.OPEN.value):
        (EyeStateCCOO.OPENING, EyeStateCCOO.CLOSED, EyeStateCCOO.OPEN)
}

stationary_to_moving_eye_states = {
    str(EyeStateCO.OPEN) + str(EyeStateCO.OPEN) + str(EyeStateCO.OPEN): EyeStateCCOO.OPEN,
    str(EyeStateCO.OPEN) + str(EyeStateCO.OPEN) + str(EyeStateCO.CLOSED): EyeStateCCOO.CLOSING,
    str(EyeStateCO.OPEN) + str(EyeStateCO.CLOSED) + str(EyeStateCO.OPEN): EyeStateCCOO.OPENING,
    str(EyeStateCO.OPEN) + str(EyeStateCO.CLOSED) + str(EyeStateCO.CLOSED): EyeStateCCOO.CLOSING,
    str(EyeStateCO.CLOSED) + str(EyeStateCO.OPEN) + str(EyeStateCO.OPEN): EyeStateCCOO.OPENING,
    str(EyeStateCO.CLOSED) + str(EyeStateCO.OPEN) + str(EyeStateCO.CLOSED): EyeStateCCOO.CLOSING,
    str(EyeStateCO.CLOSED) + str(EyeStateCO.CLOSED) + str(EyeStateCO.OPEN): EyeStateCCOO.OPENING,
    str(EyeStateCO.CLOSED) + str(EyeStateCO.CLOSED) + str(EyeStateCO.CLOSED): EyeStateCCOO.CLOSED,
}


invalid_state_transitions = {
    str(EyeStateCCOO.OPEN.value) + str(EyeStateCCOO.OPENING.value),
    str(EyeStateCCOO.CLOSED.value) + str(EyeStateCCOO.CLOSING.value)
}


def is_blinking(classifications):
    return does_sequence_exist(classifications, blink_states)


def is_invalid_transition(transitions):
    return does_sequence_exist(transitions, invalid_state_transitions)


def does_sequence_exist(sequence, dictionary):
    encoded_sequence = ''.join(str(item) for item in sequence)
    return encoded_sequence in dictionary


def stationary_to_moving_state(stationary_sequence):
    encoded_sequence = ''.join(str(item) for item in stationary_sequence)
    return stationary_to_moving_eye_states[encoded_sequence]


__stop = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ُStopped @ ' + str(__stop))
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ' + str(__stop - __start))
