from enum import Enum


class ClassificationType(Enum):
    CCOO_4 = '4-ccoo'
    ACO_3 = '3-aco'
    CO_2 = '2-co'