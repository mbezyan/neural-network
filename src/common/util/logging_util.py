import logging

__logger = None


def __init_logger():
    global __logger
    __logger = logging.getLogger()
    __logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    console_log_handler = logging.StreamHandler()
    console_log_handler.setLevel(logging.INFO)
    console_log_handler.setFormatter(formatter)
    __logger.addHandler(console_log_handler)


def logger():
    if __logger is None:
        __init_logger()
    return __logger
