from src.constants import *
import random


def balance_dataset(root_dir, delete):
    count = {
        'training': [0, 0, 0, 0],
        'validation': [0, 0, 0, 0],
        'evaluation': [0, 0, 0, 0]
    }
    for root, dirs, files in os.walk(root_dir):
        for file in files:
            group = root.split('\\')[-1]
            name = file.split('_')
            try:
                count[group][int(name[0])] += 1
            except ValueError as error:
                print(error)
            except KeyError as error:
                print(error)
    print(count)

    keep = {
        'training': [1.006, 1.003, 0.053, 0.968],
        'validation': [0.853, 1.023, 0.067, 1.008],
        'evaluation': [0.94, 1.022, 0.045, 1.011],
    }

    if delete:
        for root, dirs, files in os.walk(root_dir):
            for file in files:
                group = root.split('\\')[-1]
                name = file.split('_')
                random_number = random.random()
                try:
                    clazz = int(name[0])
                    if random_number > keep[group][clazz]:
                        os.remove(os.path.join(root, file))
                except ValueError as error:
                    print(error)
                except KeyError as error:
                    print(error)


def delete_unclassified(root_dir, last_classified_frame):
    for root, dirs, files in os.walk(root_dir):
        print(dirs)
        for file in files:
            # auwm-left-00031
            frame = file.split('.')[0].split('-')[-1]
            try:
                frame = int(frame)
                if frame > last_classified_frame:
                    os.remove(os.path.join(root, file))
            except ValueError as error:
                print(error)


dir1 = os.path.join(IMAGES_OUTPUT_DIR, '2-classes-co')
# delete_unclassified(dir1, 1800)

dir2 = os.path.join(TRAINING_VALIDATION_INPUT_DIR, '4-classes-ccoo5-20211114-135918')
balance_dataset(dir2, False)
