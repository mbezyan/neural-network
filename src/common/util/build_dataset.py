import os
from datetime import datetime
from shutil import copy
import random
from src.constants import *

# traverse root directory, and list directories as dirs and files as files
# for root, dirs, files in os.walk("../out/test1"):
#     path = root.split(os.sep)
#     print((len(path) - 1) * '---', os.path.basename(root))
#     for file in files:
#         print(file)
from src.constants import IMAGES_OUTPUT_DIR


def copy_open_to_classified_folder(root):
    for item in os.listdir(root):
        item_path = os.path.join(root, item)
        print("> " + item_path)
        if os.path.isdir(item_path):
            for subitem in os.listdir(item_path):
                subitem_path = os.path.join(item_path, subitem)
                if os.path.isfile(subitem_path):
                    os.rename(subitem_path, os.path.join(item_path, 'classified', 'open', subitem))


def combine_all_classified(classified_for_each_video_src_dir, classes, combined_classified_images_dest_dir):
    destination_root = combined_classified_images_dest_dir
    # Prepare dataset directory
    os.mkdir(destination_root)
    for key in classes.keys():
        new_dir = os.path.join(destination_root, key)
        os.mkdir(new_dir)

    image_file_postfixes = [0] * len(classes)
    for video_folder in os.listdir(classified_for_each_video_src_dir): # Each folder for images classified for each video
        if 'classified' not in video_folder:
            video_folder_path = os.path.join(classified_for_each_video_src_dir, video_folder)
            print("> " + video_folder_path)
            classified_dir = os.path.join(video_folder_path, 'classified')
            for class_dir in os.listdir(classified_dir):
                class_dir = class_dir.lower()
                file_name_prefix = classes[class_dir][0]
                class_dir_path = os.path.join(classified_dir, class_dir)
                destination = os.path.join(destination_root, class_dir)
                for image_file in os.listdir(class_dir_path):
                    image_file_path = os.path.join(class_dir_path, image_file)
                    # Replicate the file if required to increase sample size
                    replica = 1
                    replication_factor = classes[class_dir][1]
                    while replica <= replication_factor:
                        file_name_postfix = image_file_postfixes[file_name_prefix]
                        new_file_name = str(file_name_prefix) + '_' + str(file_name_postfix) + '.png'
                        copy(image_file_path, destination)
                        os.rename(os.path.join(destination, image_file), os.path.join(destination, new_file_name))
                        image_file_postfixes[file_name_prefix] += 1
                        replica += 1


def create_dataset(classified_imgs_dir, classes, dataset_dir):
    # Prepare dataset directory
    os.mkdir(dataset_dir)
    training_dir = os.path.join(dataset_dir, '../../training')
    validation_dir = os.path.join(dataset_dir, 'validation')
    evaluation_dir = os.path.join(dataset_dir, 'evaluation')
    os.mkdir(training_dir)
    os.mkdir(validation_dir)
    os.mkdir(evaluation_dir)

    for class_dir in os.listdir(classified_imgs_dir):
        print(class_dir)
        class_dir_path = os.path.join(classified_imgs_dir, class_dir)
        for image_file in os.listdir(class_dir_path):
            image_file_path = os.path.join(class_dir_path, image_file)
            random_number = random.random()
            if 0 <= random_number < classes[class_dir][2][0]:
                copy(image_file_path, training_dir)
            elif classes[class_dir][2][0] <= random_number < classes[class_dir][2][1]:
                copy(image_file_path, validation_dir)
            elif classes[class_dir][2][1] <= random_number < classes[class_dir][2][2]:
                copy(image_file_path, evaluation_dir)


# Classes
# > 3 Classes
# >> Original
# classes3 = {'open': [0, [0.0183,	0.0259,	0.0305]],
#            'almost-closed': [1, [0.5764, 0.8166, 0.9607]],
#            'closed': [2, [0.3076, 0.4358, 0.5127]]}
# >> almost-closed doubled
# classes3 = {'open': [0, [0.038, 0.0502, 0.0624]],
#            'almost-closed': [1, [0.6004, 0.7926, 0.9848]],
#            'closed': [2, [0.6107, 0.8258, 1.0009]]}
# > 4 Classes
# >> Original
# classes4 = {'open': [0, 1, [0.1061, 0.1327, 0.1593]],
#             'closing': [1, 1, [0.6065, 0.7582, 0.9099]],
#             'closed': [2, 1, [0.6368, 0.796, 0.9552]],
#             'opening': [3, 1, [0.6143, 0.7679, 0.9215]]}
# classes4 = {'closed': [0, 1, [0.4227, 0.4932, 0.5637]],
#             'closing': [1, 1, [0.7212, 0.8414, 0.9616]],
#             'open': [2, 1, [0.0324, 0.0378, 0.0432]],
#             'opening': [3, 1, [0.7286, 0.8501, 0.9716]]}

classes4 = {'closed': [0, 1, [0.7045, 0.8454, 0.9863]],
            'closing': [1, 1, [0.7212, 0.8414, 0.9616]],
            'open': [2, 1, [0.0539, 0.0647, 0.0755]],
            'opening': [3, 1, [0.7286, 0.8501, 0.9716]]}


# path = "../out/images/grayscale"
# copy_open_to_classified_folder(path)
# -----------

classification_type = '4-classes-ccoo'
classification_type = '4'
dateTimeObj = datetime.now()
timestamp = dateTimeObj.strftime('%Y%m%d-%H%M%S')
classified_for_each_video_src_dir = os.path.join('IMAGES_OUTPUT_DIR', '4-classes')
print("[INFO] Combining all classified images")
combined_classified_images_dest_dir = os.path.join(classified_for_each_video_src_dir, 'classified-' + timestamp)
dataset_dir = os.path.join(TRAINING_VALIDATION_INPUT_DIR, + '4-classes-' + timestamp)
print("[INFO] Creating training/validation/evaulation data set")
combine_all_classified('../../../out/images/4-classes', classes4, combined_classified_images_dest_dir)
create_dataset(combined_classified_images_dest_dir, classes4, dataset_dir)
