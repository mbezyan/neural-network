import sys
from src.common.util.utils import *
import csv
import src.common.eye_state as eye_state
from src.common.eye import *
from collections import deque
from src.classfication_util import calculate_blink_state_tensor, both_eyes_blinking

start_processing_frame = 1

# For each frame in video:
    # Classify frame as CLOSING/CLOSED/OPEN/OPENNING
    # If classified as CLOSING/CLOSED/OPENNING
        # Add all three frames to a set

# Store set
# For each frame in video:
    # If frame is in set
        # Store both eyes and eyebrows in blinks folder
    # Else
        # Store in OPEN folder


def detect_blinks_in_video(video_name):
    logger.info("Starting processing: " + video_name)

    video_capture_handle = video_capture_handle_for_name(video_name)

    le_running_frame = [None] * constants.number_of_imgs_to_stack
    le_last_state_transition_frame = sys.maxsize
    le_classification = eye_state.EyeStateCCOO.OPEN.value # eye_state.EyeStateCO.OPEN.value
    le_classification_label = eye_state.EyeStateCCOO.OPEN.name # eye_state.EyeStateCO.OPEN.value
    le_new_states = deque([])
    le_blinking = False
    le_blinking_frame = None

    re_running_frame = [None] * constants.number_of_imgs_to_stack
    re_last_state_transition_frame = sys.maxsize
    re_classification = eye_state.EyeStateCCOO.OPEN.value # eye_state.EyeStateCO.OPEN.value
    re_classification_label = eye_state.EyeStateCCOO.OPEN.name # eye_state.EyeStateCO.OPEN.value
    re_new_states = deque([])
    re_blinking = False
    re_blinking_frame = None

    blink_frames = set()

    # For each frame until the end of the video
    frame = 0
    frames_processed = 0
    while True: # and frame < 100:
        frame += 1
        if frame % 500 == 0:
            logger.info("Video frame: " + str(frame))

        # Image
        ret, image = video_capture_handle.read()

        # When ret is False, it means there are no more frames
        if ret is False:
            logger.info("Reached end of video")
            break

        if frame < start_processing_frame:
            continue

        try:
            left_eye, right_eye, under_le_coords, under_re_coords = extract_LEEB_REEB_from_image(image)
        except NoFacialLandmarksFound:
            # Go to next frame and skip this one
            logger.error("Frame " + str(frame) + " skipped because no landmarks were found")
            continue
        except LeftEyeEyebrowNotFound:
            logger.error("Frame " + str(frame) + " skipped because left eye was not detected")
            continue
        except RightEyeEyebrowNotFound:
            logger.error("Frame " + str(frame) + " skipped because right eye was not detected")
            continue

        # Keep two previous frames in memory
        keep_previous_frames_in_memory(le_running_frame, re_running_frame, left_eye, right_eye)

        # From frame 3 onwards start merging with the two previous frame
        if frames_processed >= constants.number_of_imgs_to_stack:
            le_classification, le_classification_label, le_last_state_transition_frame, le_new_states, le_blinking = \
                calculate_blink_state_tensor(Eye.LEFT, frame, le_running_frame, le_last_state_transition_frame,
                                             le_classification, le_new_states)
            re_classification, re_classification_label, re_last_state_transition_frame, re_new_states, re_blinking = \
                calculate_blink_state_tensor(Eye.RIGHT, frame, re_running_frame, re_last_state_transition_frame,
                                             re_classification, re_new_states)

        if le_blinking:
            le_blinking_frame = frame
        if re_blinking:
            re_blinking_frame = frame
        if both_eyes_blinking(le_blinking_frame, re_blinking_frame):
            logger.info("L+R: " + str(frame) + "[" + str(le_blinking_frame) + ", " + str(re_blinking_frame) + "]")

            blink_frames.add(le_blinking_frame - 10)
            blink_frames.add(le_blinking_frame - 9)
            blink_frames.add(le_blinking_frame - 8)
            blink_frames.add(le_blinking_frame - 7)
            blink_frames.add(le_blinking_frame - 6)
            blink_frames.add(le_blinking_frame - 5)
            blink_frames.add(le_blinking_frame - 4)
            blink_frames.add(le_blinking_frame - 3)
            blink_frames.add(le_blinking_frame - 2)
            blink_frames.add(le_blinking_frame - 1)
            blink_frames.add(le_blinking_frame)
            blink_frames.add(le_blinking_frame + 1)

            blink_frames.add(re_blinking_frame - 10)
            blink_frames.add(re_blinking_frame - 9)
            blink_frames.add(re_blinking_frame - 8)
            blink_frames.add(re_blinking_frame - 7)
            blink_frames.add(re_blinking_frame - 6)
            blink_frames.add(re_blinking_frame - 5)
            blink_frames.add(re_blinking_frame - 4)
            blink_frames.add(re_blinking_frame - 3)
            blink_frames.add(re_blinking_frame - 2)
            blink_frames.add(re_blinking_frame - 1)
            blink_frames.add(re_blinking_frame)
            blink_frames.add(re_blinking_frame + 1)

            le_blinking_frame = None
            re_blinking_frame = None

        frames_processed += 1

    logger.debug(blink_frames)
    output_file_path = os.path.join(constants.CSV_OUTPUT_DIR, video_name + '-' + timestamp() + '-potential-blink-frames.csv')
    with open(output_file_path, "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(list(blink_frames))

    return output_file_path


def extract_save_blink_images(video_name, blink_frames_csv_file):
    blink_frames = None
    with open(blink_frames_csv_file, newline='') as f:
        reader = csv.reader(f)
        blink_frames = list(reader)

    blink_frames = set(int(x) for x in blink_frames[0])
    # video_capture_handle.set(cv2.cv2.CAP_PROP_POS_FRAMES, 0)
    video_capture_handle = video_capture_handle_for_name(video_name)

    # Create directory for output
    file_dir_name = video_name + "-" + timestamp()
    images_dir_name = os.path.join(constants.IMAGES_OUTPUT_DIR, file_dir_name)
    os.mkdir(images_dir_name)
    os.mkdir(os.path.join(images_dir_name, 'blinking'))
    os.mkdir(os.path.join(images_dir_name, 'not-blinking'))

    # For each frame until the end of the video
    frame = 0
    while True: # and frame < 100:
        frame += 1
        if frame % 500 == 0:
            logger.info("Video frame: " + str(frame))

        # Image
        ret, image = video_capture_handle.read()

        # When ret is False, it means there are no more frames
        if ret is False:
            logger.info("Reached end of video")
            break

        if frame < start_processing_frame:
            continue

        try:
            left_right_eye = extract_LEEB_REEB_from_image_as_one(image)
        except NoFacialLandmarksFound:
            # Go to next frame and skip this one
            logger.error("Frame " + str(frame) + " skipped because no landmarks were found")
            continue
        except LeftEyeEyebrowNotFound:
            logger.error("Frame " + str(frame) + " skipped because left eye was not detected")
            continue
        except RightEyeEyebrowNotFound:
            logger.error("Frame " + str(frame) + " skipped because right eye was not detected")
            continue

        if frame in blink_frames:
            image_path = os.path.join(images_dir_name,  'blinking', str(frame).zfill(5) + '.png')
        else:
            image_path = os.path.join(images_dir_name,  'not-blinking', str(frame).zfill(5) + '.png')
        left_right_eye_resized = cv2.resize(left_right_eye, (270, 130))
        cv2.imwrite(image_path, left_right_eye_resized)


def main():
    video_name = '38-black-female3-VxulD6o1a5g'
    output_file_path = detect_blinks_in_video(video_name)
    extract_save_blink_images('38-black-female3-VxulD6o1a5g', output_file_path)


main()
