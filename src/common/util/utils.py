import timeit
import os
from PIL import Image
import cv2
from numpy import array, dstack
from src import constants
from src.common import mp_constants
import mediapipe as mp
from datetime import datetime
from src.common.custom_exceptions import \
    NoFacialLandmarksFound, LeftEyeEyebrowNotFound, RightEyeEyebrowNotFound, FileNotFoundException
from src.common.util.logging_util import logger
import inspect

__face_mesh = None

__start = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': Started @ ' + str(__start))


def __init_face_mesh():
    global __face_mesh
    mp_face_mesh = mp.solutions.face_mesh
    __face_mesh = mp_face_mesh.FaceMesh(max_num_faces=1)


def face_mesh_instance():
    if __face_mesh is None:
        __init_face_mesh()
    return __face_mesh


def video_capture_handle_for_name(video_name):
    video_extension = constants.videos[video_name]['extension']
    video_file_name = video_name + ('.' + video_extension if video_extension is not None else '')
    video_file_path = os.path.join(constants.VIDEOS_INPUT_DIR, video_file_name)
    video_capture_handle_for_path(video_file_path)


def video_capture_handle_for_path(video_file_path):
    if os.path.isfile(video_file_path):
        return cv2.VideoCapture(video_file_path)
    else:
        logger().error('Video not found %s', video_file_path)
        raise FileNotFoundException


def three_grayscale_imgs_to_one_rgb(images, reverse=True):
    ar1 = array(images[0])
    ar2 = array(images[1])
    ar3 = array(images[2])
    if reverse:
        return Image.fromarray(dstack((ar3, ar2, ar1)), 'RGB')
    else:
        return Image.fromarray(dstack((ar1, ar2, ar3)), 'RGB')


def save_eye_image(image, frame, dir, img_name_prefix):
    image.save(os.path.join(dir, img_name_prefix + str(frame).zfill(5) + '.png'))


# Extract and returns images of left eye and eyebrow (LEEB) and right eye and eyebrow (REEB)
def extract_LEEB_REEB_from_image(image, prep_for_classification_or_stacking=True):
    start = timeit.default_timer()
    function_name = __file__ + '.' + inspect.stack()[0][3]
    logger().debug("[PROFILING] " + function_name + ': Started @ ' + str(start))

    # Right
    rv1 = None  # rurx
    rh1 = None  # rury
    rv2 = None  # rllx
    rh2 = None  # rlly

    # Left
    lv1 = None  # lurx
    lh1 = None  # lury
    lv2 = None  # lllx
    lh2 = None  # llly

    height, width, _ = image.shape
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    image_landmarks = face_mesh_instance().process(image)

    # Facial landmarks
    if image_landmarks.multi_face_landmarks is None:
        raise NoFacialLandmarksFound

    facial_landmarks = image_landmarks.multi_face_landmarks[0]  # Assuming only one face in the frame

    # i = 0
    # landmark_coordinates = [None] * mp_constants.NUMBER_OF_LANDMARKS
    landmarks = facial_landmarks.landmark
    rv1 = int(landmarks[mp_constants.RIGHT_EYE_EYEBROW_UPPER_RIGHT_INDICES[0]].x * width)
    rh1 = int(landmarks[mp_constants.RIGHT_EYE_EYEBROW_UPPER_RIGHT_INDICES[1]].y * height)
    rh2 = int(landmarks[mp_constants.RIGHT_EYE_EYEBROW_LOWER_LEFT_INDICES[0]].y * height)
    rv2 = int(landmarks[mp_constants.RIGHT_EYE_EYEBROW_LOWER_LEFT_INDICES[1]].x * width)
    lv1 = rv2
    lh1 = int(landmarks[mp_constants.LEFT_EYE_EYEBROW_UPPER_RIGHT_INDICES[1]].y * height)
    lv2 = int(landmarks[mp_constants.LEFT_EYE_EYEBROW_LOWER_LEFT_INDICES[0]].x * width)
    lh2 = int(landmarks[mp_constants.LEFT_EYE_EYEBROW_LOWER_LEFT_INDICES[1]].y * height)

    under_LE_x = int(landmarks[mp_constants.UNDER_LE_INDEX].x * width)
    under_LE_y = int(landmarks[mp_constants.UNDER_LE_INDEX].y * height)
    under_RE_x = int(landmarks[mp_constants.UNDER_RE_INDEX].x * width)
    under_RE_y = int(landmarks[mp_constants.UNDER_RE_INDEX].y * height)

    left_eye = image[lh1:lh2, lv1:lv2]
    right_eye = image[rh1:rh2, rv1:rv2]

    if prep_for_classification_or_stacking:
        try:
            left_eye = cv2.cvtColor(left_eye, cv2.COLOR_BGR2GRAY)
            left_eye = cv2.resize(left_eye, constants.MIN_DIMENSIONS_FOR_GOOGLENET)
        except Exception as error:
            print(error)
            raise LeftEyeEyebrowNotFound

        try:
            right_eye = cv2.cvtColor(right_eye, cv2.COLOR_BGR2GRAY)
            right_eye = cv2.resize(right_eye, constants.MIN_DIMENSIONS_FOR_GOOGLENET)
        except Exception as error:
            print(error)
            raise RightEyeEyebrowNotFound

    return_value = left_eye, right_eye, \
                   (under_RE_x, under_RE_y), \
                   (under_LE_x, under_LE_y), \
                   [[lv1, lh1, lv2, lh2], [rv1, rh1, rv2, rh2]]

    stop = timeit.default_timer()
    logger().debug("[PROFILING] " + function_name + ': ُStopped @ ' + str(stop))
    logger().debug("[PROFILING] " + function_name + ': ' + str(stop - start))

    return return_value


# rurx = None
# rury = None
# rllx = None
3


# rlly = None
#
# # Left
# lurx = None
# lury = None
# lllx = None
# llly = None


# Extract and returns images of left eye and eyebrow (LEEB) and right eye and eyebrow (REEB)
def extract_LEEB_REEB_from_image_as_one(image):
    # Right
    rurx = None
    # rury = None
    # rllx = None
    rlly = None

    # Left
    # lurx = None
    lury = None
    lllx = None
    # llly = None

    height, width, _ = image.shape

    image_landmarks = face_mesh_instance().process(image)

    # Facial landmarks
    if image_landmarks.multi_face_landmarks is None:
        raise NoFacialLandmarksFound

    facial_landmarks = image_landmarks.multi_face_landmarks[0]  # Assuming only one face in the frame

    i = 0
    landmark_coordinates = [None] * mp_constants.NUMBER_OF_LANDMARKS
    for landmark in facial_landmarks.landmark:
        pt1 = landmark
        x = int(pt1.x * width)
        y = int(pt1.y * height)
        if i == mp_constants.RIGHT_EYE_EYEBROW_UPPER_RIGHT_INDICES[0]:
            rurx = x
        # elif i == mp_constants.RIGHT_EYE_EYEBROW_UPPER_RIGHT_INDICES[1]:
        #     rury = y
        elif i == mp_constants.RIGHT_EYE_EYEBROW_LOWER_LEFT_INDICES[0]:
            rlly = y
        # elif i == mp_constants.RIGHT_EYE_EYEBROW_LOWER_LEFT_INDICES[1]:
        #     rllx = x
        #     lurx = x
        elif i == mp_constants.LEFT_EYE_EYEBROW_UPPER_RIGHT_INDICES[1]:
            lury = y
        elif i == mp_constants.LEFT_EYE_EYEBROW_LOWER_LEFT_INDICES[0]:
            lllx = x
        # elif i == mp_constants.LEFT_EYE_EYEBROW_LOWER_LEFT_INDICES[1]:
        #     llly = y
        landmark_coordinates[i] = (x, y)
        i += 1

    left_right_eye = image[lury:rlly, rurx:lllx]
    return left_right_eye


def keep_previous_frames_in_memory(LE_running_frame, RE_running_frame, left_eye, right_eye):
    LE_running_frame[2] = LE_running_frame[1]
    LE_running_frame[1] = LE_running_frame[0]
    LE_running_frame[0] = left_eye
    RE_running_frame[2] = RE_running_frame[1]
    RE_running_frame[1] = RE_running_frame[0]
    RE_running_frame[0] = right_eye


def timestamp():
    date_time_obj = datetime.now()
    return date_time_obj.strftime('%Y%m%d-%H%M%S')


__stop = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ُStopped @ ' + str(__stop))
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ' + str(__stop - __start))
