def approximate_facial_landmark_polygon_area(polygon_lines, face_mesh_coordinates):
    area = 0
    for polygon_line in polygon_lines:
        # Average of heights of the points in a line
        average_height = (face_mesh_coordinates[polygon_line[0]][1] + face_mesh_coordinates[polygon_line[1]][1])/2
        width = face_mesh_coordinates[polygon_line[1]][0] - face_mesh_coordinates[polygon_line[0]][0]
        area_under_line = average_height * width
        area += area_under_line

    return area
