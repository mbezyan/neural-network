import os
from utils import *
from shutil import copy
import random
from src.constants import *
from src.common.eye_state import EyeStateCCOO, EyeStateACO, EyeStateCO
from src.constants import IMAGES_OUTPUT_DIR


classification_params = {
    '4-classes-ccoo': {
        'classes': EyeStateCCOO,
        'sets': {
            'training': [
                '01-asian-female-20210904-150928',
                '02-australian-white-male1-M0gGLCXR1n0-20210904-151132',
                '03-black-female1-k2jiK8zWsb0-20210904-151220',
                '05-black-male-news-reader-XMrxsEDtyTQ-20210904-151533',
                '07-caucasian-male1-youtube-_hjfQ41LF0w-20210904-153133',
                '08-caucasian-male-news-reader-wZNng505VYM-20210904-153326',
                '09-eastern-european-female-youtube-Tjom_yAUj-s-20210904-152147',
                '10-indian-female1-ZD6jw2FvWaE-20210904-152357',
                '11-indian-male1-c9F5kMUfFKk-20210904-153541',
                '12-man-glasses-monitor-reflection-20210904-153809',
                '13-mohammad-reading-20210904-152609',
                '15-white-male-bald-round-face-uwPtFbA1sRY-20210904-152948',
                '17-cacusian-male-EACDJJW3aqo-20210916-221707'
            ],
            'validation': [
                '04-black-female2-SskrmHCoiRc-20210904-151434',
                '06-blond-white-male-uwPtFbA1sRY-20210904-152041',
                '14-white-female-news-interview-XMrxsEDtyTQ-20210904-152817'
            ],
            'evaluation': [
                '16-african-canadian-french-female-glasses-mGWzHsgld58-20210914-164517',
                '18-australia-white-male-glasses-Y6QtKgB8K3Q-20210917-211109',
                '19-indian-male-5j7KxVTNBdY-20210918-224431'
            ]
        }
    },
    '4-classes-ccoo2': {
        'classes': EyeStateCCOO,
        'sets': {
            'training': [
                '01-asian-female-20211004-154738',
                '02-australian-white-male1-M0gGLCXR1n0-20211004-150742',
                '03-black-female1-k2jiK8zWsb0-20211004-150830',
                '05-black-male-news-reader-XMrxsEDtyTQ-20211004-151145',
                '07-caucasian-male1-youtube-_hjfQ41LF0w-20211004-151404',
                '08-caucasian-male-news-reader-wZNng505VYM-20211004-151556',
                '09-eastern-european-female-youtube-Tjom_yAUj-s-20211004-154938',
                '10-indian-female1-ZD6jw2FvWaE-20211004-155150',
                '11-indian-male1-c9F5kMUfFKk-20211004-155412',
                '12-man-glasses-monitor-reflection-20211004-163605',
                '13-mohammad-reading-20211004-163819',
                '15-white-male-bald-round-face-uwPtFbA1sRY-20211004-164156',
                '17-cacusian-male-EACDJJW3aqo-20211004-164552'
            ],
            'validation': [
                '04-black-female2-SskrmHCoiRc-20211004-151047',
                '06-blond-white-male-uwPtFbA1sRY-20211004-151300',
                '14-white-female-news-interview-XMrxsEDtyTQ-20211004-164024'
            ],
            'evaluation': [
                '16-african-canadian-french-female-glasses-mGWzHsgld58-20211004-164340',
                '18-australia-white-male-glasses-Y6QtKgB8K3Q-20211004-164754',
                '19-indian-male-5j7KxVTNBdY-20211004-193120'
            ]
        }
    },
    '4-classes-ccoo3': {
        'classes': EyeStateCCOO,
        'sets': {
            'training': [
                '01-asian-female-20211005-205133',
                '02-australian-white-male1-M0gGLCXR1n0-20211005-210001',
                '03-black-female1-k2jiK8zWsb0-20211005-210054',
                '05-black-male-news-reader-XMrxsEDtyTQ-20211005-210733',
                '07-caucasian-male1-youtube-_hjfQ41LF0w-20211005-210953',
                '08-caucasian-male-news-reader-wZNng505VYM-20211005-211153',
                '09-eastern-european-female-youtube-Tjom_yAUj-s-20211005-211643',
                '10-indian-female1-ZD6jw2FvWaE-20211005-212326',
                '11-indian-male1-c9F5kMUfFKk-20211005-212658',
                '12-man-glasses-monitor-reflection-20211005-213119',
                '13-mohammad-reading-20211005-213410',
                '15-white-male-bald-round-face-uwPtFbA1sRY-20211005-213917',
                '17-cacusian-male-EACDJJW3aqo-20211005-214350'
            ],
            'validation': [
                '04-black-female2-SskrmHCoiRc-20211005-210633',
                '06-blond-white-male-uwPtFbA1sRY-20211005-210850',
                '14-white-female-news-interview-XMrxsEDtyTQ-20211005-213741'
            ],
            'evaluation': [
                '16-african-canadian-french-female-glasses-mGWzHsgld58-20211005-214112',
                '18-australia-white-male-glasses-Y6QtKgB8K3Q-20211005-214606',
                '19-indian-male-5j7KxVTNBdY-20211005-214806'
            ]
        }
    },
    '4-classes-ccoo4': {
        'classes': EyeStateCCOO,
        'sets': {
            'training': [
                '01-asian-female-20211016-120540',
                '02-australian-white-male1-M0gGLCXR1n0-20211016-120747',
                '03-black-female1-k2jiK8zWsb0-20211016-120833',
                '05-black-male-news-reader-XMrxsEDtyTQ-20211016-121206',
                '07-caucasian-male1-youtube-_hjfQ41LF0w-20211016-121436',
                '08-caucasian-male-news-reader-wZNng505VYM-20211016-121631',
                '09-eastern-european-female-youtube-Tjom_yAUj-s-20211016-121838',
                '10-indian-female1-ZD6jw2FvWaE-20211016-122052',
                '11-indian-male1-c9F5kMUfFKk-20211016-122324',
                '12-man-glasses-monitor-reflection-20211016-122550',
                '13-mohammad-reading-20211016-122817',
                '15-white-male-bald-round-face-uwPtFbA1sRY-20211016-123222',
                '17-cacusian-male-EACDJJW3aqo-20211016-123638',
                '20-white-obese-male1-TQHW7gGjrCQ-20211016-124257',
                '21-hispanic-female1-20211016-124509',
            ],
            'validation': [
                '04-black-female2-SskrmHCoiRc-20211016-121101',
                '06-blond-white-male-uwPtFbA1sRY-20211016-121328',
                '14-white-female-news-interview-XMrxsEDtyTQ-20211016-123037',
            ],
            'evaluation': [
                '16-african-canadian-french-female-glasses-mGWzHsgld58-20211016-123420',
                '18-australia-white-male-glasses-Y6QtKgB8K3Q-20211016-123843',
                '19-indian-male-5j7KxVTNBdY-20211016-124038',
            ]
        }
    },
    '4-classes-ccoo5': {
        'classes': EyeStateCCOO,
        'sets': {
            'training': [
                '01-asian-female-20211017-140736',
                '02-australian-white-male1-M0gGLCXR1n0-20211017-140958',
                '03-black-female1-k2jiK8zWsb0-20211017-141050',
                '05-black-male-news-reader-XMrxsEDtyTQ-20211017-141406',
                '07-caucasian-male1-youtube-_hjfQ41LF0w-20211017-141620',
                '08-caucasian-male-news-reader-wZNng505VYM-20211017-141809',
                '09-eastern-european-female-youtube-Tjom_yAUj-s-20211017-142014',
                '10-indian-female1-ZD6jw2FvWaE-20211017-142224',
                '11-indian-male1-c9F5kMUfFKk-20211017-142449',
                '12-man-glasses-monitor-reflection-20211017-142712',
                '13-mohammad-reading-20211017-142938',
                '15-white-male-bald-round-face-uwPtFbA1sRY-20211017-143344',
                '17-cacusian-male-EACDJJW3aqo-20211017-143816',
                '20-white-obese-male1-TQHW7gGjrCQ-20211017-144414',
                '21-hispanic-female1-Qlu_7ZcbSW0-20211017-144714',
                '23-caucasian-american-female1-aIvRs-M8n8o-20211106-172526',
                '24-caucasian-american-female2-aIvRs-M8n8o-20211106-172805',
                '27-caucasian-male2-XCjosBT3Gy8-20211107-165320',
                '28-caucasian-middle-aged-male-X2mVL_trjmo-20211107-165603',
                '29-caucasian-young-male-angle1-zgwMnvPbzuQ-20211113-130816',
                '31-cacucasian-male-bald-glasses1-wIjK-6Do6lg-20211113-181115',
                '35-caucasian-old-american-male1-eI65CbmTlAc-20211114-125756',
            ],
            'validation': [
                '04-black-female2-SskrmHCoiRc-20211017-141309',
                '06-blond-white-male-uwPtFbA1sRY-20211017-141520',
                '14-white-female-news-interview-XMrxsEDtyTQ-20211017-143159',
                '25-white-german-male1-7YHZ2qXXPxw-20211106-173047',
                '30-asian-female2-kayOhGRcNt4-20211113-131054',
                '33-indian-male-glasses1-DqNb0png0gA-20211113-182223',
            ],
            'evaluation': [
                '16-african-canadian-french-female-glasses-mGWzHsgld58-20211017-143549',
                '18-australia-white-male-glasses-Y6QtKgB8K3Q-20211017-144020',
                '19-indian-male-5j7KxVTNBdY-20211017-144214',
                '26-while-female-eye-makeup2-eE1kAY87UbM-20211106-173214',
                '32-caucasian-young-male2-oocNVvTHP5M-20211113-181910',
                '34-caucasian-middle-aged-female-light-makeup1-oYFCbCZF8hA-20211114-122619',
            ]
        }
    },
    '3-classes-aco': {
        'classes': EyeStateACO,
        'sets': {
            'training': [
                '01-asian-female-20210814-161723',
                '02-australian-white-male1-M0gGLCXR1n0-20210814-223757',
                '03-black-female1-k2jiK8zWsb0-20210814-173924',
                '05-black-male-news-reader-XMrxsEDtyTQ-20210814-171135',
                '07-caucasian-male1-youtube-_hjfQ41LF0w-20210814-161629',
                '08-caucasian-male-news-reader-wZNng505VYM-20210814-165418',
                '09-eastern-european-female-youtube-Tjom_yAUj-s-20210814-163347',
                '10-indian-female1-ZD6jw2FvWaE-20210814-224005',
                '11-indian-male1-c9F5kMUfFKk-20210814-224222',
                '12-man-glasses-monitor-reflection-20210814-161510',
                '13-mohammad-reading-20210814-161339',
                '15-white-male-bald-round-face-uwPtFbA1sRY-20210814-174232',
                '17-cacusian-male-EACDJJW3aqo-20210914-165324'
            ],
            'validation': [
                '04-black-female2-SskrmHCoiRc-20210814-223909',
                '06-blond-white-male-uwPtFbA1sRY-20210814-174158',
                '14-white-female-news-interview-XMrxsEDtyTQ-20210814-170621'
            ],
            'evaluation': [
                '16-african-canadian-french-female-glasses-mGWzHsgld58-20210913-064152',
                '18-australia-white-male-glasses-Y6QtKgB8K3Q-20210916-222353',
                '19-indian-male-5j7KxVTNBdY-20210918-172909'
            ]
        }
    },
    '2-classes-co': {
        'classes': EyeStateCO,
        'sets': {
            'training': [
                '01-asian-female-20210814-161723',
                '02-australian-white-male1-M0gGLCXR1n0-20210814-223757',
                '03-black-female1-k2jiK8zWsb0-20210814-173924',
                '05-black-male-news-reader-XMrxsEDtyTQ-20210814-171135',
                '07-caucasian-male1-youtube-_hjfQ41LF0w-20210814-161629',
                '08-caucasian-male-news-reader-wZNng505VYM-20210814-165418',
                '09-eastern-european-female-youtube-Tjom_yAUj-s-20210814-163347',
                '10-indian-female1-ZD6jw2FvWaE-20210814-224005',
                '11-indian-male1-c9F5kMUfFKk-20210814-224222',
                '12-man-glasses-monitor-reflection-20210814-161510',
                '13-mohammad-reading-20210814-161339',
                '15-white-male-bald-round-face-uwPtFbA1sRY-20210814-174232',
                '17-cacusian-male-EACDJJW3aqo-20210914-165324',
                '20-white-obese-male1-TQHW7gGjrCQ-20211009-110725',
                '21-hispanic-female1-Qlu_7ZcbSW0-20211009-140747',
                '23-caucasian-american-female1-aIvRs-M8n8o-20211106-102354',
                '24-caucasian-american-female2-aIvRs-M8n8o-20211106-103259',
                '27-caucasian-male2-XCjosBT3Gy8-20211107-155821',
                '28-caucasian-middle-aged-male-X2mVL_trjmo-20211107-162033',

            ],
            'validation': [
                '04-black-female2-SskrmHCoiRc-20210814-223909',
                '06-blond-white-male-uwPtFbA1sRY-20210814-174158',
                '14-white-female-news-interview-XMrxsEDtyTQ-20210814-170621',
                '25-white-german-male1-7YHZ2qXXPxw-20211106-104135',
            ],
            'evaluation': [
                '16-african-canadian-french-female-glasses-mGWzHsgld58-20210913-064152',
                '18-australia-white-male-glasses-Y6QtKgB8K3Q-20210916-222353',
                '19-indian-male-5j7KxVTNBdY-20210918-172909',
                '26-while-female-eye-makeup2-eE1kAY87UbM-20211106-104653',
            ]
        }
    }
}


def create_dataset(classification_type):
    params = classification_params[classification_type]
    classes = params['classes']
    # Create dataset directory
    dataset_dir = os.path.join(TRAINING_VALIDATION_INPUT_DIR, classification_type + '-' + timestamp())
    os.mkdir(dataset_dir)
    training_dir = os.path.join(dataset_dir, 'training')
    validation_dir = os.path.join(dataset_dir, 'validation')
    evaluation_dir = os.path.join(dataset_dir, 'evaluation')
    os.mkdir(training_dir)
    os.mkdir(validation_dir)
    os.mkdir(evaluation_dir)

    # Indices for image file names
    image_file_postfixes = [0] * len(classes)

    for key, value in params['sets'].items():
        for video_folder in value:  # Each folder for images classified for each video
            video_folder_path = os.path.join(IMAGES_OUTPUT_DIR, classification_type, video_folder)
            print("> " + video_folder_path)
            classified_dir = os.path.join(video_folder_path, 'classified')
            for class_dir in os.listdir(classified_dir):
                class_dir = class_dir.lower()
                file_name_prefix = classes[class_dir.upper()].value
                class_dir_path = os.path.join(classified_dir, class_dir)
                destination = os.path.join(dataset_dir, key)
                for image_file in os.listdir(class_dir_path):
                    image_file_path = os.path.join(class_dir_path, image_file)
                    file_name_postfix = image_file_postfixes[file_name_prefix]
                    new_file_name = str(file_name_prefix) + '_' + str(file_name_postfix) + '.png'
                    copy(image_file_path, destination)
                    # print(image_file_path + " --C--> " + destination)
                    os.rename(os.path.join(destination, image_file), os.path.join(destination, new_file_name))
                    # print(os.path.join(destination, image_file) + " --R--> " + os.path.join(destination, new_file_name))
                    image_file_postfixes[file_name_prefix] += 1


# create_dataset('4-classes-ccoo')
# create_dataset('4-classes-ccoo2')
# create_dataset('4-classes-ccoo3')
# create_dataset('4-classes-ccoo4')
create_dataset('4-classes-ccoo5')
# create_dataset('3-classes-aco')
# create_dataset('2-classes-co')
