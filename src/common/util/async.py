import asyncio
import time


async def say_after(delay, what):
    await asyncio.sleep(delay)
    print(what)


async def main():
    print(f"started at {time.strftime('%X')}")

    task1 = asyncio.create_task(
        say_after(5, 'hello'))

    task2 = asyncio.create_task(
        say_after(6, 'world'))



    # Wait until both tasks are completed (should take
    # around 2 seconds.)
    print("1")
    await task1
    print("2")
    await task2
    print("3")

    time.sleep(4)

    print(f"finished at {time.strftime('%X')}")


asyncio.run(main())
