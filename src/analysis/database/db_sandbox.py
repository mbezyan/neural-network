import psycopg2
from psycopg2 import Error
import cv2
import numpy as np
import timeit

try:
    # Connect to an existing database
    connection = psycopg2.connect(user="postgres",
                                  password="R0b3rtT1mm$",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="db-sandbox1")

    # Create a cursor to perform database operations
    cursor = connection.cursor()
    image_cursor = connection.cursor()
    # Print PostgreSQL details
    print("PostgreSQL server information")
    print(connection.get_dsn_parameters(), "\n")
    # Executing a SQL query
    cursor.execute("SELECT version();")
    # Fetch result
    record = cursor.fetchone()
    print("You are connected to - ", record, "\n")
    total_db_id_time = 0
    fetch_ids_time = 0
    fetch_images_time = 0

    print("Going to run query")
    __start = timeit.default_timer()
    # cursor.execute("Select id, image from participant_image order by timestamp ASC")
    cursor.execute("Select id, image from participant_image where session_id = '4ea05d2fc8e243e088e91597b9375bc5' order by timestamp ASC")
    __stop = timeit.default_timer()
    fetch_ids_time = __stop - __start
    record_count = cursor.rowcount
    print("Query found " + str(record_count) + " records in " + str(fetch_ids_time) + " seconds")
    for image_mv in cursor:
        # __start = timeit.default_timer()
        # image_cursor.execute("Select image from participant_image where id = '" + image_mv[0] + "'")
        # __stop = timeit.default_timer()
        # fetch_images_time += __stop - __start
        # image_record = image_cursor.fetchone()
        # image = image_record[0]#.tobytes()

        recordId = image_mv[0]
        image = image_mv[1]
        # print(recordId)

        __start = timeit.default_timer()
        nparr = np.frombuffer(image, np.uint8)
        __stop = timeit.default_timer()
        from_buffer = __stop - __start

        __start = timeit.default_timer()
        img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        __stop = timeit.default_timer()
        imdecode = __stop - __start

        print("frombuffer: " + str(from_buffer) + ", imdecode: " + str(imdecode))

        # Show image
        # cv2.imshow("Image", img_np)
        # cv2.waitKey(1)

    print(record_count)
    print(fetch_ids_time)
    print(fetch_images_time)
    print(fetch_images_time / record_count)





except (Exception, Error) as error:
    print("Error while connecting to PostgreSQL", error)
finally:
    if connection:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
