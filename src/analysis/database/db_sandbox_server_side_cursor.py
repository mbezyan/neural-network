import os

import psycopg2
from psycopg2 import Error
import cv2
import numpy as np
import timeit

from src.common.eye import Eye
from src.common.eye_state import EYE_STATE_CCOO_FROM_STRING
from src.common.util.logging_util import logger
import src.common.util.utils as util
from src import constants
from src.classfication_util import calc_eye_tensor_state, is_blinking, both_eyes_blinking
from src.types.classification.Classification import Classification
import uuid

from src.types.classification.helpers.IsBlinkingIn import IsBlinkingIn
from src.types.classification.helpers.IsBlinkingOut import IsBlinkingOut
from src.types.eye.Blink import Blink

ITERSIZE = 1000
DATABASE_CONFIG = {
    "user": "postgres",
    "password": "****",
    "host": "127.0.0.1",
    "port": "5432",
    "database": "db-sandbox1"
}
FRAMES_PER_SECOND = 30
MILLIS_IN_SECOND = 1000
MILLIS_BETWEEN_FRAMES = MILLIS_IN_SECOND / FRAMES_PER_SECOND


def get_connection():
    return psycopg2.connect(user=DATABASE_CONFIG['user'],
                            password=DATABASE_CONFIG['password'],
                            host=DATABASE_CONFIG['host'],
                            port=DATABASE_CONFIG['port'],
                            database=DATABASE_CONFIG['database'])


def close_connection_cursor(connection, cursors):
    if connection:
        for cursor in cursors:
            if cursor is not None:
                cursor.close()
        connection.close()
        logger().info("PostgreSQL connection is closed")


def handle_db_error(error):
    print("Error while connecting to PostgreSQL", error)


def image_from_pg_bytea(image_bytea):
    numpy_array_1d = np.frombuffer(image_bytea, np.uint8)
    decoded = cv2.imdecode(numpy_array_1d, cv2.IMREAD_COLOR)
    return decoded


def pg_bytea_from_image(image):
    is_success, im_buf_arr = cv2.imencode(".png", image)
    byte_im = im_buf_arr.tobytes()
    return byte_im


def eyes_from_image(image):
    left_eye, right_eye = \
        util.extract_LEEB_REEB_from_image(image, prep_for_classification_or_stacking=False)[0:2]
    left_eye = cv2.cvtColor(left_eye, cv2.COLOR_BGR2RGB)
    right_eye = cv2.cvtColor(right_eye, cv2.COLOR_BGR2RGB)
    return left_eye, right_eye


def show_images(session_id):
    cursor_name = 'image_cursor'
    itersize = ITERSIZE
    query = "Select id, shot, left_eye, right_eye from participant_image where session_id = %s order by timestamp ASC"
    params = (session_id,)

    try:
        connection = get_connection()
        with connection:
            with connection.cursor(name=cursor_name) as cursor:
                cursor.itersize = itersize
                query = query
                cursor.execute(query, params)
                for record in cursor:
                    record_id = record[0]
                    logger().info(record_id)
                    frame = record[1]
                    left_eye = record[2]
                    right_eye = record[3]

                    frame = image_from_pg_bytea(frame)
                    # print("frame:", str(type(frame)))
                    # frame_numpy_array_1d = np.frombuffer(frame, np.uint8)
                    # print("frombuffer:", str(type(frame_numpy_array_1d)))
                    # frame_decoded = cv2.imdecode(frame_numpy_array_1d, cv2.IMREAD_COLOR)
                    # print("cv2.imdecode:", str(type(frame_decoded)))
                    cv2.imshow("Frame", frame)

                    if left_eye is not None:
                        left_eye = image_from_pg_bytea(left_eye)
                        # print("left_eye:", str(type(left_eye)))
                        # left_eye_numpy_array_1d = np.frombuffer(left_eye, np.uint8)
                        # print("frombuffer:", str(type(left_eye_numpy_array_1d)))
                        # left_eye_decoded = cv2.imdecode(left_eye_numpy_array_1d, cv2.IMREAD_COLOR)
                        # print("cv2.imdecode:", str(type(left_eye_decoded)))
                        cv2.imshow("Left", left_eye)

                    if right_eye is not None:
                        right_eye = image_from_pg_bytea(right_eye)
                        # print("right_eye:", str(type(right_eye)))
                        # print("left_eye:", str(type(right_eye)))
                        # right_eye_numpy_array_1d = np.frombuffer(right_eye, np.uint8)
                        # print("frombuffer:", str(type(right_eye_numpy_array_1d)))
                        # right_eye_decoded = cv2.imdecode(right_eye_numpy_array_1d, cv2.IMREAD_COLOR)
                        # print("cv2.imdecode:", str(type(right_eye_decoded)))+
                        cv2.imshow("Right", right_eye)

                    cv2.waitKey(1)

    except (Exception, Error) as error:
        handle_db_error(error)

    finally:
        close_connection_cursor(connection, [cursor])


def extract_and_persist_eyes_from_images(session_id):
    cursor_name = 'image_cursor'
    itersize = ITERSIZE
    query = "Select id, shot from participant_image where session_id = %s order by timestamp ASC"
    params = (session_id,)

    # Other cursors
    update_image_cursor = None

    try:
        connection = get_connection()
        with connection:
            with connection.cursor(name=cursor_name) as cursor:
                cursor.itersize = itersize
                query = query
                cursor.execute(query, params)
                for record in cursor:
                    record_id = record[0]
                    frame = record[1]
                    image = image_from_pg_bytea(frame)
                    left_eye, right_eye = eyes_from_image(image)

                    # Persist images
                    binary_left_eye = pg_bytea_from_image(left_eye)
                    binary_right_eye = pg_bytea_from_image(right_eye)
                    update_image_cursor = connection.cursor()
                    update_image_cursor.execute(
                        """
                        UPDATE participant_image
                        SET left_eye = (%s),
                        right_eye = (%s)
                        WHERE id = (%s)
                        """,
                        (binary_left_eye, binary_right_eye, record_id)
                    )

    except (Exception, Error) as error:
        handle_db_error(error)

    finally:
        cursors = [cursor, update_image_cursor]
        close_connection_cursor(connection, cursors)


def count_video_frames(video_file_path):
    video_capture_handle = util.video_capture_handle_for_path(video_file_path)
    frames = 0
    while True:
        ret, image = video_capture_handle.read()
        # When ret is False, it means there are no more frames
        if ret is False:
            logger().info("Reached end of video: " + str(frames))
            return frames
        else:
            frames += 1
            if frames % 500 == 0:
                logger().info("Counted " + str(frames) + " so far ...")


def extract_and_persist_eyes_from_video(session_id):
    connection = get_connection()
    read_video_details_cursor = None
    insert_image_cursor = None
    try:
        # Retrieve video file ID and start timestamp
        with connection:
            read_video_details_cursor = connection.cursor()
            query = "Select video_file_id, video_start_ts from lab_session where id = %s"
            params = (session_id,)
            read_video_details_cursor.execute(query, params)
            record = read_video_details_cursor.fetchone()  # There must only be 1 record since retrieving by primary key
            video_file_id = record[0]
            video_start_ts = record[1]
            video_file_path = os.path.join(constants.LAB_SESSION_VIDEO_DIR, video_file_id)

        logger().info("Video has " + str(count_video_frames(video_file_path)) + " frames.")

        video_capture_handle = util.video_capture_handle_for_path(video_file_path)
        frame = 0
        while True:
            # Image
            ret, image = video_capture_handle.read()
            # When ret is False, it means there are no more frames
            if ret is False:
                logger().info("Reached end of video: " + str(frame))
                break
            frame += 1
            if frame % 100 == 0:
                logger().info("Processed " + str(frame) + " so far ...")

            # Do something with image
            left_eye, right_eye = eyes_from_image(image)
            binary_left_eye = pg_bytea_from_image(left_eye)
            binary_right_eye = pg_bytea_from_image(right_eye)
            record_id = uuid.uuid4().hex
            timestamp = (MILLIS_BETWEEN_FRAMES * (frame - 1)) + video_start_ts
            with connection:
                insert_image_cursor = connection.cursor()
                insert_image_cursor.execute(
                    """
                    INSERT INTO 
                    participant_image(id, session_id, left_eye, right_eye, frame_number, timestamp)
                    VALUES ((%s), (%s), (%s), (%s), (%s), (%s))                
                    """,
                    (record_id, session_id, binary_left_eye, binary_right_eye, frame, timestamp)
                )

    except (Exception, Error) as error:
        handle_db_error(error)

    finally:
        cursors = [read_video_details_cursor, insert_image_cursor]
        close_connection_cursor(connection, cursors)


def prep_for_stacking_classification(image):
    grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return cv2.resize(grayscale, constants.MIN_DIMENSIONS_FOR_GOOGLENET)


def calculate_eye_states_for_session(session_id):
    cursor_name = 'image_cursor'
    itersize = ITERSIZE
    query = "Select id, left_eye, right_eye from participant_image where session_id = %s order by timestamp ASC"
    params = (session_id,)

    try:
        connection = get_connection()
        with connection:
            with connection.cursor(name=cursor_name) as cursor:
                cursor.itersize = itersize
                query = query
                cursor.execute(query, params)

                le_running_frame = [None] * constants.number_of_imgs_to_stack
                re_running_frame = [None] * constants.number_of_imgs_to_stack
                frames_processed = 0
                le_tensor_class_prev = Classification()
                re_tensor_class_prev = Classification()
                frame = 0
                for record in cursor:
                    frame += 1
                    if frame % 100 == 0:
                        logger().info("Processed " + str(frame) + " so far ...")

                    record_id = record[0]
                    left_eye = image_from_pg_bytea(record[1])
                    right_eye = image_from_pg_bytea(record[2])

                    # Prepare images for stacking and classification
                    left_eye = prep_for_stacking_classification(left_eye)
                    right_eye = prep_for_stacking_classification(right_eye)

                    util.keep_previous_frames_in_memory(le_running_frame, re_running_frame, left_eye, right_eye)
                    frames_processed += 1

                    # From frame 3 onwards start merging with the two previous frame
                    if frames_processed >= constants.number_of_imgs_to_stack:
                        le_tensor_class = calc_eye_tensor_state(le_running_frame, le_tensor_class_prev)
                        re_tensor_class = calc_eye_tensor_state(re_running_frame, re_tensor_class_prev)

                        # Persist classifications and probabilities
                        update_record_cursor = connection.cursor()
                        update_record_cursor.execute(
                            """
                            UPDATE participant_image
                            SET
                            left_eye_tensor_state = (%s),
                            left_eye_tensor_state_probability = (%s),
                            right_eye_tensor_state = (%s),
                            right_eye_tensor_state_probability = (%s)                        
                            WHERE id = (%s)
                            """,
                            (
                                le_tensor_class.label,
                                le_tensor_class.probability.item(),
                                re_tensor_class.label,
                                re_tensor_class.probability.item(),
                                record_id
                            )
                        )

                        le_tensor_class_prev = le_tensor_class
                        re_tensor_class_prev = re_tensor_class

    except (Exception, Error) as error:
        handle_db_error(error)

    finally:
        cursors = []
        if cursor is not None:
            cursors.append(cursor)
        if update_record_cursor is not None:
            cursors.append(update_record_cursor)
        close_connection_cursor(connection, cursors)


def view_video(video_name):
    video_capture_handle = util.video_capture_handle_for_name(video_name)
    frames = 0
    while True:
        # Image
        ret, image = video_capture_handle.read()
        # When ret is False, it means there are no more frames
        if ret is False:
            logger().info("Reached end of video: " + str(frames))
            break
        frames += 1
        if frames % 300 == 0:
            print(frames)
        cv2.imshow("Image", image)
        cv2.waitKey(1)


def calculate_blinks_for_session(session_id):
    cursor_name = 'image_cursor'
    itersize = ITERSIZE
    query = "Select frame_number, timestamp, left_eye_tensor_state, right_eye_tensor_state from participant_image where session_id = %s order by frame_number ASC"
    params = (session_id,)

    try:
        connection = get_connection()
        insert_update_cursor = None
        with connection:
            with connection.cursor(name=cursor_name) as cursor:
                cursor.itersize = itersize
                query = query
                cursor.execute(query, params)

                # Some initialisation
                eyes_states = []  # e.g. [[1, 'open', 'open'], [2, 'open', 'open'], ...]
                frame_timestamps = []  # Index 0 reps frame 1, i1 reps f2, etc. e.g. [1643673702912, 1643673702945, ...]

                for record in cursor:
                    frame = record[0]
                    timestamp = record[1]
                    left_eye_state = record[2]
                    right_eye_state = record[3]
                    eyes_states.append([frame, left_eye_state, right_eye_state])
                    frame_timestamps.append(timestamp)

            blinking_frames_list = calculate_blinks(eyes_states)
            # Persist blinks and blink analysis
            # Some initialisation
            previous_blink_start_ts = None
            insert_update_cursor = connection.cursor()
            for sequence, blinking_frames in enumerate(blinking_frames_list, start=1):
                insert_update_cursor.execute(
                    """
                    UPDATE participant_image
                    SET
                    blinking = (%s)                      
                    WHERE session_id = (%s) AND frame_number IN %s
                    """,
                    (True, session_id, blinking_frames)
                )

                # For blink analysis
                record_id = uuid.uuid4().hex
                start_frame = min(blinking_frames)
                start_ts = frame_timestamps[start_frame - 1]
                end_frame = max(blinking_frames)
                end_ts = frame_timestamps[end_frame - 1]
                duration = end_ts - start_ts
                interval = (start_ts - previous_blink_start_ts) if previous_blink_start_ts is not None else None
                previous_blink_start_ts = start_ts

                insert_update_cursor.execute(
                    """
                    INSERT INTO 
                    blink_analysis(id, session_id, sequence, start_ts, start_frame, end_ts, end_frame, duration, interval)
                    VALUES ((%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s))                
                    """,
                    (record_id, session_id, sequence, start_ts, start_frame, end_ts, end_frame, duration, interval)
                )

    except (Exception, Error) as error:
        handle_db_error(error)

    finally:
        cursors = []
        if cursor is not None:
            cursors.append(cursor)
        if insert_update_cursor is not None:
            cursors.append(insert_update_cursor)
        close_connection_cursor(connection, cursors)


def calculate_blinks(records):
    # Return object: List of tuples
    blinking_frames_list = []

    # Some initialisation
    le_is_blinking_in = IsBlinkingIn(eye=Eye.LEFT)
    le_blinking_frame = None

    re_is_blinking_in = IsBlinkingIn(eye=Eye.RIGHT)
    re_blinking_frame = None

    for record in records:
        frame = record[0]
        if frame % 100 == 0:
            logger().info("Processed " + str(frame) + " frames so far ...")
        left_eye_state = None if record[1] is None else EYE_STATE_CCOO_FROM_STRING[record[1]].value
        right_eye_state = None if record[2] is None else EYE_STATE_CCOO_FROM_STRING[record[2]].value

        le_is_blinking_in.frame = frame
        le_is_blinking_in.clazz = left_eye_state
        le_is_blinking_out = is_blinking(le_is_blinking_in)

        re_is_blinking_in.frame = frame
        re_is_blinking_in.clazz = right_eye_state
        re_is_blinking_out = is_blinking(re_is_blinking_in)

        # Set previous before next iteration
        le_is_blinking_in.previous_clazz = left_eye_state
        re_is_blinking_in.previous_clazz = right_eye_state

        if le_is_blinking_out.blink.blinking:
            le_blinking_frame = frame
        if re_is_blinking_out.blink.blinking:
            re_blinking_frame = frame
        if both_eyes_blinking(le_blinking_frame, re_blinking_frame):
            logger().info(
                "L+R: " + str(frame) + "[" + str(le_blinking_frame) + ", " + str(re_blinking_frame) + "]")
            le_blinking_frame = None
            re_blinking_frame = None
            blinking_frames = tuple(dict.fromkeys(le_is_blinking_out.blink.frames + re_is_blinking_out.blink.frames))
            blinking_frames_list.append(blinking_frames)
            blink_intensity = max(le_is_blinking_out.blink.intensity, re_is_blinking_out.blink.intensity)

            le_is_blinking_in.blink = Blink()
            re_is_blinking_in.blink = Blink()

    return blinking_frames_list


def check_output(expected, actual):
    if expected == actual:
        return "[Passed]: " + str(actual)
    else:
        return "[FAILED]: Expected " + str(expected) + ", Received " + str(actual)


# ------------------------------------ Testing -------------------------------------------------------------------------

sid = 'b75b426453b74b35a96d16d6f9807048'
vid = 'f90320ba8018d09fe6ab874dd903a9f7'
vid_path = 'C:/comp8800/mind-lab/mind-lab-study-api1/upload/1ca9d893a90fd739c46e6a40a31dacbe'

# Unit tests
# Simple
blinking_frames_list1 = calculate_blinks([[1, 'open', 'open'], [2, 'closed', 'closed'], [3, 'open', 'open'], [4, 'open', 'open']])
logger().info(check_output([(1, 2, 3)], blinking_frames_list1))

blinking_frames_list1 = calculate_blinks([[1, 'open', 'open'], [2, 'open', 'open'], [3, 'closed', 'closed'], [4, 'open', 'open'], [5, 'open', 'open'], [6, 'open', 'open']])
logger().info(check_output([(2, 3, 4)], blinking_frames_list1))

# Eyes not in sync
blinking_frames_list1 = calculate_blinks([[1, 'open', 'open'], [2, 'open', 'closing'], [3, 'closing', 'closed'], [4, 'closed', 'opening'], [5, 'opening', 'open'], [6, 'open', 'open']])
logger().info(check_output([(1, 2, 3, 4, 5)], blinking_frames_list1))

# Long blink
blinking_frames_list1 = calculate_blinks([[1, 'open', 'open'], [2, 'closing', 'closing'], [3, 'closing', 'closing'], [4, 'closed', 'closed'], [5, 'closed', 'closed'], [6, 'opening', 'opening'], [7, 'opening', 'opening'], [8, 'open', 'open']])
logger().info(check_output([(1, 2, 3, 4, 5, 6)], blinking_frames_list1))

# Test plumbing
# show_images(sid)

# View video
# view_video(vid)

# Count video frames
# count_video_frames(vid_path)


def run_blink_extraction_analysis_pipeline(session_id, is_video=True):
    if is_video:
        # 1a- Extract left and right eyes from video for a given session
        extract_and_persist_eyes_from_video(session_id)
    else:
        # 1b- Extract left and right eyes from images stored in database
        extract_and_persist_eyes_from_images(session_id)

    # 2- Calculate eye tensor states for a give session
    calculate_eye_states_for_session(sid)

    # 3- For a given session
    calculate_blinks_for_session(sid)


# Pipeline
run_blink_extraction_analysis_pipeline(sid)
