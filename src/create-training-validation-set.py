from datetime import datetime
from src.constants import *

from distutils.dir_util import copy_tree

import os


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file),
                       os.path.relpath(os.path.join(root, file),
                                       os.path.join(path, '..')))


dateTimeObj = datetime.now()
timestamp = dateTimeObj.strftime('%Y%m%d-%H%M%S')

training_validation_group = "27-classes"

training_validation_set_dir = TRAINING_VALIDATION_INPUT_DIR + training_validation_group + "-" + timestamp + '/'
training_validation_set_training_dir = training_validation_set_dir + "training/"
training_validation_set_validation_dir = training_validation_set_dir + "validation/"

# Create folders
os.mkdir(training_validation_set_dir)
os.mkdir(training_validation_set_training_dir)
os.mkdir(training_validation_set_validation_dir)
# os.mkdir(training_validation_set_training_dir + EyeState.OPEN.name)
# os.mkdir(training_validation_set_training_dir + EyeState.CLOSING.name)
# os.mkdir(training_validation_set_training_dir + EyeState.CLOSED.name)
# os.mkdir(training_validation_set_training_dir + EyeState.OPENING.name)
# os.mkdir(training_validation_set_validation_dir + EyeState.OPEN.name)
# os.mkdir(training_validation_set_validation_dir + EyeState.CLOSING.name)
# os.mkdir(training_validation_set_validation_dir + EyeState.CLOSED.name)
# os.mkdir(training_validation_set_validation_dir + EyeState.OPENING.name)

# Define training and validation set
training_set = [
    '01-asian-female-20210814-161723',
    '02-australian-white-male1-M0gGLCXR1n0-20210814-223757',
    '03-black-female1-k2jiK8zWsb0-20210814-173924',
    '04-black-female2-SskrmHCoiRc-20210814-223909',
    '05-black-male-news-reader-XMrxsEDtyTQ-20210814-171135',
    '06-blond-white-male-uwPtFbA1sRY-20210814-174158',
    '09-eastern-european-female-youtube-Tjom_yAUj-s-20210814-163347',
    '10-indian-female1-ZD6jw2FvWaE-20210814-224005',
    '13-mohammad-reading-20210814-161339',
    '14-white-female-news-interview-XMrxsEDtyTQ-20210814-170621',
    '15-white-male-bald-round-face-uwPtFbA1sRY-20210814-174232',
]
validation_set = [
    '07-caucasian-male1-youtube-_hjfQ41LF0w-20210814-161629',
    '08-caucasian-male-news-reader-wZNng505VYM-20210814-165418',
    '11-indian-male1-c9F5kMUfFKk-20210814-224222',
    '12-man-glasses-monitor-reflection-20210814-161510'
]

# Copy files
for item in training_set:
    print("Copying from " + IMAGES_OUTPUT_DIR + item + " to " + training_validation_set_training_dir)
    copy_tree(IMAGES_OUTPUT_DIR + 'grayscale/' + item + '/' + 'classified/', training_validation_set_training_dir)

for item in validation_set:
    print("Copying from " + IMAGES_OUTPUT_DIR + item + " to " + training_validation_set_validation_dir)
    copy_tree(IMAGES_OUTPUT_DIR + 'grayscale/' + item + '/' + 'classified', training_validation_set_validation_dir)


# zipf = zipfile.ZipFile(training_validation_group + "-" + timestamp + '.zip', 'w', zipfile.ZIP_DEFLATED)
# zipdir(TRAINING_VALIDATION_INPUT_DIR, zipf)
# zipf.close()