import csv
import os
# Load CSV file into array
# Frame	LE area	Closed/Open?	State	RE area	Closed/Open?	State
# 1	0.642583732		Open	0.626248217		Open
eye_states = {}
left_right_index = {'left': 0, 'right': 1}
csv_file_path = '../out/csv/19-indian-male-5j7KxVTNBdY-20210918-172909.csv'
with open(csv_file_path) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    next(csv_reader, None)
    for row in csv_reader:
        eye_states[int(row[0])] = [row[3], row[6]]

# Directory containing images
images_dir = '../out/images/19-indian-male-5j7KxVTNBdY-20210918-172909'

# Classes
OPEN = 'open'
CLOSED = 'closed'
UNKNOWN = '#N/A'
CLASSIFIED = 'classified'

# Create directories for classes
os.mkdir(os.path.join(images_dir, CLASSIFIED))
open_dir = os.path.join(images_dir, CLASSIFIED, OPEN)
os.mkdir(open_dir)
closed_dir = os.path.join(images_dir, CLASSIFIED, CLOSED)
os.mkdir(closed_dir)


# Iterate through images and if classified as Open/Closed move to appropriate directory
for item in os.listdir(images_dir):
    item_path = os.path.join(images_dir, item)
    if os.path.isfile(item_path):
        # 16bf-left-00001.png
        file_name_parts = item.split('-')
        left_right = file_name_parts[1]
        frame = int(file_name_parts[2].split('.')[0])
        eye_state = eye_states[frame][left_right_index[left_right]]
        if eye_state != UNKNOWN:
            os.rename(item_path, os.path.join(images_dir, CLASSIFIED, eye_state.lower(), item))
