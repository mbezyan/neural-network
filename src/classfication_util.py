import numpy as np
from tensorflow.keras.applications.vgg16 import preprocess_input
# from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.applications import VGG16
# from tensorflow.keras.applications import InceptionV3
from tensorflow.keras.preprocessing.image import img_to_array
import pickle
import cv2
from tensorflow.keras.models import load_model
import src.common.util.utils as utils
from src.common.util.logging_util import logger
import src.constants as constants
import src.common.eye_state as eye_state
import timeit
import os
import inspect

from src.types.classification.Classification import Classification
from src.types.classification.helpers.IsBlinkingOut import IsBlinkingOut
from src.types.eye.Blink import Blink

__start = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': Started @ ' + str(__start))

MODEL_ROOT = constants.TRAINING_DIR
LOG_REG_ROOT = os.path.join(MODEL_ROOT, 'transfer_learning_keras_feature_extraction')
LOG_REG_MODEL_FILE = 'model.cpickle'
FINE_TUNED_CNN_ROOT = os.path.join(MODEL_ROOT, 'transfer_learning_keras_fine_tuning')
FINE_TUNED_CNN_MODEL_FILE = 'eyes.model'

classification_input_type = 'single-grayscale-frame'
async_eye_blink_threshold = 3
show_video = True
delay_between_frames = 0
min_uniq_states_for_blink = 3  # e.g. OPEN -> CLOSED -> OPEN
max_uniq_states_for_blink = 4  # e.g. OPEN -> CLOSING -> CLOSED -> OPENING
# If the eye remains in one state for a second (30 frames) or more then it's not blinking
max_frames_for_blink = 30
start_processing_frame = 500

MEAN_FOR_CNN_CLASSIFICATION = np.array([123.68, 116.779, 103.939][::1], dtype="float32")

__models = None

PREFERRED_TENSOR_FINE_TUNED_CNN_MODEL = 'ccoo4-fine-tuned-cnn'


def __init_models(classification_type):
    global __models
    # Specify trained models, paths and classes
    __models = {
        'co2-fine-tuned-cnn': {
            'path': os.path.join(FINE_TUNED_CNN_ROOT, 'output-' + '2-classes-co-20211009-221104',
                                 FINE_TUNED_CNN_MODEL_FILE),
            'model': None,
            'classes': constants.CLASSES[constants.ClassificationType.CO_2],
            'model-serialisation': 'tensorflow-keras',
        },
        'co2-logistic-regression': {
            # 'path': os.path.join(LOG_REG_ROOT, 'output-' + '2-classes-co-20211017-214639-2', LOG_REG_MODEL_FILE),
            'path': os.path.join(LOG_REG_ROOT, 'output-' + '2-classes-co-20211107-221604-vgg16', LOG_REG_MODEL_FILE),
            'model': None,
            'classes': constants.CLASSES[constants.ClassificationType.CO_2],
            'model-serialisation': 'pickle',
        },
        'aco3-fine-tuned-cnn': {
            'path': os.path.join(FINE_TUNED_CNN_ROOT, 'output-' + '3-classes-aco-20211003-162609',
                                 FINE_TUNED_CNN_MODEL_FILE),
            'model': None,
            'classes': constants.CLASSES[constants.ClassificationType.ACO_3],
            'model-serialisation': 'tensorflow-keras',
        },
        'aco3-logistic-regression': {
            'path': os.path.join(LOG_REG_ROOT, 'output-' + '3-classes-aco-20211003-162609', LOG_REG_MODEL_FILE),
            'model': None,
            'classes': constants.CLASSES[constants.ClassificationType.ACO_3],
            'model-serialisation': 'pickle',
        },
        'ccoo4-fine-tuned-cnn': {
            # 'path': os.path.join(FINE_TUNED_CNN_ROOT, 'output-' + '4-classes-ccoo5-20211017-145556', FINE_TUNED_CNN_MODEL_FILE),
            'path': os.path.join(FINE_TUNED_CNN_ROOT, 'output-' + '4-classes-ccoo5-20211114-135918-50-20',
                                 FINE_TUNED_CNN_MODEL_FILE),
            'model': None,
            'classes': constants.CLASSES[constants.ClassificationType.CCOO_4],
            'model-serialisation': 'tensorflow-keras',
        },
        'ccoo4-logistic-regression': {
            'path': os.path.join(LOG_REG_ROOT, 'output-' + '4-classes-ccoo-20211003-161758', LOG_REG_MODEL_FILE),
            'model': None,
            'classes': constants.CLASSES[constants.ClassificationType.CCOO_4],
            'model-serialisation': 'pickle',
        }
    }

    value = __models[classification_type]
    if value['model-serialisation'] == 'tensorflow-keras':
        value['model'] = load_model(value['path'])
    elif value['model-serialisation'] == 'pickle':
        value['model'] = pickle.load(open(value['path'], 'rb'))

    # Load the trained Logistic Regression model from disk
    # logger().info("Loading trained models")
    # for key, value in __models.items():
    #     logger().info("Loading " + key)
    #     if value['model-serialisation'] == 'tensorflow-keras':
    #         value['model'] = load_model(value['path'])
    #     elif value['model-serialisation'] == 'pickle':
    #         value['model'] = pickle.load(open(value['path'], 'rb'))


def __get_models(classification_type):
    if __models is None:
        __init_models(classification_type)
    return __models


# Load the VGG16 network
__pre_trained_cnn = None


def __init_pre_trained_cnn():
    global __pre_trained_cnn
    logger().info("Loading pre-trained CNN models")
    __pre_trained_cnn = VGG16(weights="imagenet", include_top=False)


def __pre_trained_cnn_inst():
    if __pre_trained_cnn is None:
        __init_pre_trained_cnn()
    return __pre_trained_cnn


# models['aco3-logistic-regression']['model'] = pickle.load(open(models['aco3-logistic-regression']['path'], 'rb'))
# models['ccoo4-logistic-regression']['model'] = pickle.load(open(models['ccoo4-logistic-regression']['path'], 'rb'))
#

# # Load fine-tuned CNN model from disk
# logger().info("Loading fine-tuned CNN models")
# models['aco3-fine-tuned-cnn']['model'] = load_model(models['aco3-fine-tuned-cnn']['path'])
# models['ccoo4-fine-tuned-cnn']['model'] = load_model(models['ccoo4-fine-tuned-cnn']['path'])


def cnn_logistic_regression_classify(image, classification_type):
    image = img_to_array(image)
    # preprocess the image by
    # (1) expanding the dimensions
    # (2) subtracting the mean RGB pixel intensity from the
    # ImageNet dataset
    image = np.expand_dims(image, axis=0)
    image = preprocess_input(image)

    features = __pre_trained_cnn_inst().predict(image)
    features = features.reshape((features.shape[0], 7 * 7 * 512))
    # features = features.reshape((features.shape[0], 5 * 5 * 2048))

    # pass the image through the network to obtain our predictions
    preds = __get_models(classification_type)[classification_type]['model'].predict(features)
    classification = int(preds[0])
    label = __get_models(classification_type)[classification_type]['classes'][classification]
    return classification, label


def cnn_classify(image, classification_type):
    start = timeit.default_timer()
    function_name = __file__ + '.' + inspect.stack()[0][3]
    logger().debug("[PROFILING] " + function_name + ': Started @ ' + str(start))

    step0 = timeit.default_timer()
    model = __get_models(classification_type)[classification_type]['model']
    classes = __get_models(classification_type)[classification_type]['classes']
    expanded_image = np.expand_dims(image, axis=0)

    image = img_to_array(image)
    step1 = timeit.default_timer()
    logger().debug("[PROFILING] " + '- step1: ' + str(step1 - step0))
    # our model was trained on RGB ordered images but OpenCV represents
    # images in BGR order, so swap the channels, and then resize to
    # 224x224 (the input dimensions for VGG16)
    # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # convert the image to a floating point data type and perform mean
    # subtraction
    image = image.astype("float32") - MEAN_FOR_CNN_CLASSIFICATION
    step2 = timeit.default_timer()
    logger().debug("[PROFILING] " + '- step2: ' + str(step2 - step1))

    # pass the image through the network to obtain our predictions
    preds = model.predict(expanded_image)[0]
    step3 = timeit.default_timer()
    logger().debug("[PROFILING] " + '- step3: ' + str(step3 - step2))

    classification = np.argmax(preds)
    step4 = timeit.default_timer()
    logger().debug("[PROFILING] " + '- step4: ' + str(step4 - step3))

    # Change to most likely, i.e. OPEN if certainty is less than 80%
    # if classification == 0 and preds[classification] < 0.8:
    #     classification = 2

    label = classes[classification]
    probability = preds[classification]
    step5 = timeit.default_timer()
    logger().debug("[PROFILING] " + '- step5: ' + str(step5 - step4))

    stop = timeit.default_timer()
    logger().debug("[PROFILING] " + function_name + ': ُStopped @ ' + str(stop))
    logger().debug("[PROFILING] " + function_name + ': ' + str(stop - start))

    return classification, label, probability


def calc_blink_state_grayscale(eye, frame, eye_image, last_state_transition_frame, previous_classification, new_states):
    classification, classification_label = cnn_logistic_regression_classify(eye_image, 'co2-logistic-regression')
    # classification, classification_label = cnn_classify(eye_images_merged, 'ccoo4-fine-tuned-cnn')
    # If transition is not allowed, then change classification back to previous classification
    # if eye_state.is_invalid_transition([previous_classification, classification]):
    #     classification = previous_classification
    #     classification_label = constants.CLASSES[ClassificationType.CO_2][classification]
    blinking = False
    if classification != previous_classification:
        last_state_transition_frame = frame
        # If we have not seen a state transition recently, add the previous classification as the first state
        if not new_states:
            new_states.append(previous_classification)
        new_states.append(classification)
        if min_uniq_states_for_blink <= len(new_states) <= max_uniq_states_for_blink:
            logger().debug(eye.value + "? " + str(frame) + " " + str(new_states))
            blinking = list(new_states) == [1, 0, 1]
            if blinking:
                logger().debug(eye.value + "+ " + str(frame) + " " + str(new_states))
                new_states.clear()
            else:
                if len(new_states) >= max_uniq_states_for_blink:
                    new_states.popleft()
    else:
        if frame - last_state_transition_frame > max_frames_for_blink:
            new_states.clear()

    return classification, classification_label, last_state_transition_frame, new_states, blinking


def calc_eye_tensor_state(running_frame, previous_tensor_state):
    eye_images_merged = utils.three_grayscale_imgs_to_one_rgb(running_frame, reverse=False)
    clazz, label, probability = cnn_classify(eye_images_merged, PREFERRED_TENSOR_FINE_TUNED_CNN_MODEL)
    if eye_state.is_invalid_transition([previous_tensor_state.clazz, clazz]):
        return previous_tensor_state
    else:
        return Classification(clazz, label, probability)


def is_blinking(inp):  # inp is input
    if inp.clazz != inp.previous_clazz:
        inp.last_state_transition = inp.frame
        # If we have not seen a state transition recently, add the previous classification as the first state
        if not inp.new_states:
            inp.new_states.append(inp.previous_clazz)
            inp.blink.frames.append(inp.frame - 1)  # Blink may have started at the previous frame
        inp.new_states.append(inp.clazz)
        inp.blink.frames.append(inp.frame)
        if min_uniq_states_for_blink <= len(inp.new_states) <= max_uniq_states_for_blink:
            logger().debug(inp.eye.value + "? " + str(inp.frame) + " " + str(inp.new_states))
            blinking = eye_state.is_blinking(inp.new_states)
            if blinking:
                logger().debug(inp.eye.value + "+ " + str(inp.frame) + " " + str(inp.new_states))
                inp.blink.intensity = 1 if eye_state.EyeStateCCOO.CLOSED.value in inp.new_states else 0.5
                inp.blink.blinking = True
                inp.new_states.clear()
            else:
                if len(inp.new_states) >= max_uniq_states_for_blink:
                    inp.new_states.popleft()
                    inp.blink.frames.popleft()
    else:
        no_change_in_state_count = inp.frame - inp.last_state_transition
        if no_change_in_state_count > max_frames_for_blink or inp.clazz == eye_state.EyeStateCCOO.OPEN.value:
            inp.new_states.clear()
            inp.blink = Blink()
        else:
            inp.blink.frames.append(inp.frame)

    return IsBlinkingOut(inp.last_state_transition, inp.new_states, inp.blink)


def calculate_blink_state_tensor(eye, frame, running_frame, last_state_transition_frame, previous_classification,
                                 new_states, blinking_frames):
    blink_intensity = 0
    eye_images_merged = utils.three_grayscale_imgs_to_one_rgb(running_frame)
    # classification, classification_label = cnn_logistic_regression_classify(eye_images_merged)
    classification, classification_label = cnn_classify(eye_images_merged, PREFERRED_TENSOR_FINE_TUNED_CNN_MODEL)[0:2]
    # If transition is not allowed, then change classification back to previous classification
    if eye_state.is_invalid_transition([previous_classification, classification]):
        classification = previous_classification
        classification_label = constants.CLASSES[constants.ClassificationType.CCOO_4][classification]
    blinking = False
    if classification != previous_classification:
        last_state_transition_frame = frame
        # If we have not seen a state transition recently, add the previous classification as the first state
        if not new_states:
            new_states.append(previous_classification)
            blinking_frames.append(frame - 1)
        new_states.append(classification)
        blinking_frames.append(frame)
        if min_uniq_states_for_blink <= len(new_states) <= max_uniq_states_for_blink:
            logger().debug(eye.value + "? " + str(frame) + " " + str(new_states))
            blinking = eye_state.is_blinking(new_states)
            if blinking:
                logger().debug(eye.value + "+ " + str(frame) + " " + str(new_states))
                blink_intensity = 1 if eye_state.EyeStateCCOO.CLOSED.value in new_states else 0.5
                new_states.clear()
            else:
                if len(new_states) >= max_uniq_states_for_blink:
                    new_states.popleft()
                    blinking_frames.popleft()
    else:
        if (frame - last_state_transition_frame) > max_frames_for_blink or \
                classification == eye_state.EyeStateCCOO.OPEN.value:
            new_states.clear()
            blinking_frames.clear()

    return classification, classification_label, last_state_transition_frame, new_states, blinking, blinking_frames, \
           blink_intensity


def both_eyes_blinking(le_blinking_frame, re_blinking_frame):
    return \
        le_blinking_frame is not None and \
        re_blinking_frame is not None and \
        (
                le_blinking_frame == re_blinking_frame or
                abs(le_blinking_frame - re_blinking_frame) <= async_eye_blink_threshold
        )


__stop = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ُStopped @ ' + str(__stop))
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ' + str(__stop - __start))
