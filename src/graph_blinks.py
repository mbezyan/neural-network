# Imports for plotting
import matplotlib.pyplot as plt
from threading import Thread
from matplotlib.gridspec import GridSpec
# import matplotlib.image as mpimg

# Plotting functions
# fig, axs = plt.subplots(4)

# Set up formatting for the movie files
# Writer = animation.writers['ffmpeg']
# writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

plotted_frame = 0
xlim_min = 0
xlim_max = 100
axs = 4 * [None]
fig = plt.figure(constrained_layout=False)
gs = GridSpec(3, 2, figure=fig)
axs[0] = fig.add_subplot(gs[0, 0])
axs[1] = fig.add_subplot(gs[0, 1])
axs[2] = fig.add_subplot(gs[1, 1])
axs[3] = fig.add_subplot(gs[2, :])
xdata, ydata = [], []
ln, = axs[3].plot([], [])


def init_plot():
    print("Init called")
    axs[3].set_xlim(xlim_min, xlim_max)
    axs[3].set_ylim(-0.1, 1.1)
    return ln, axs


def update_plot(x):
    global plotted_frame, xlim_min, xlim_max
    # print("Update called: " + str(plotted_frame))
    # if plotted_frame > (xlim - 10):
    if plotted_frame < len(frame_blink_for_plotting) - 1:
        plotted_frame += 1
        xdata.append(frame_blink_for_plotting[plotted_frame][0])
        ydata.append(frame_blink_for_plotting[plotted_frame][1][0])
        ln.set_data(xdata, ydata)
        xlim_min = 0 if plotted_frame < 50 else plotted_frame - 50
        xlim_max = plotted_frame + 50
        axs[3].set_xlim(xlim_min, xlim_max)
        # ax.relim()
        # axs[0].imshow(frame_blink_for_plotting[plotted_frame][1][1])
        # axs[1].imshow(frame_blink_for_plotting[plotted_frame][1][2])
        # axs[2].imshow(frame_blink_for_plotting[plotted_frame][1][3])
        frame_blink_for_plotting[plotted_frame-1] = None
    return ln, axs

# ani = FuncAnimation(fig,
#                     update_plot,
#                     # frames=(x for x in range(0, 1000*2)),
#                     # frames=np.linspace(0, finish*2, finish*2),
#                     init_func=init_plot,
#                     interval=1,
#                     blit=False,
#                     repeat=False)

DetectBlinksInVideo().start()
print("DetectBlinksInVideo thread started")

# thread3 = threading.Thread(target=plt.show())
# thread3.start()
# print("Plot show thread started")

# saving to m4 using ffmpeg writer
# print("Saving video")
#writervideo = animation.FFMpegWriter(fps=60)
#DetectBlinksInVideo().join()
#ani.save('increasingStraightLine.mp4', writer=writervideo)
print("Finishing")
# ani.finish()
print("Closing plotting")
# plt.close()
# ani = None
print("Plotting closed")