# import the necessary packages
import os
from src.constants import *

# initialize the path to the *original* input directory of images
set_name = '4-classes-ccoo5-20211114-135918'
ORIG_INPUT_DATASET = os.path.join(TRAINING_VALIDATION_INPUT_DIR, set_name)

# initialize the base path to the *new* directory that will contain
# our images after computing the training and testing split
BASE_PATH = "dataset" + '-' + set_name

# define the names of the training, testing, and validation
# directories
TRAIN = "training"
TEST = "evaluation"
VAL = "validation"

# initialize the list of class label names
# CLASSES = ['closed', 'open']
# CLASSES = ['almost_closed', 'closed', 'open']
CLASSES = ['closed', 'closing', 'open', 'opening']

# set the batch size when fine-tuning
BATCH_SIZE = 32

HEAD_TRAINING_EPOCHS: Final = 50 # Original value = 50
FINAL_CONV_AND_FC_TRAINING_EPOCHS: Final = 20 # Original value = 20

# initialize the label encoder file path and the output directory to
# where the extracted features (in CSV file format) will be stored
OUTPUT = "output" + '-' + set_name + '-' + str(HEAD_TRAINING_EPOCHS) + '-' + str(FINAL_CONV_AND_FC_TRAINING_EPOCHS)
LE_PATH = os.path.sep.join([OUTPUT, "le.cpickle"])
BASE_CSV_PATH = OUTPUT

# set the path to the serialized model after training
MODEL_PATH = os.path.sep.join([OUTPUT, "eyes.model"])

# define the path to the output training history plots
UNFROZEN_PLOT_PATH = os.path.sep.join([OUTPUT, "unfrozen.png"])
WARMUP_PLOT_PATH = os.path.sep.join([OUTPUT, "warmup.png"])
