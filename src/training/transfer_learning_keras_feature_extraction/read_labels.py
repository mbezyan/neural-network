from pyimagesearch import config
import pickle
import os

# load the label encoder from disk
LE_PATH = os.path.sep.join(["output_vgg16", "le.cpickle"])
le = pickle.loads(open(LE_PATH, "rb").read())
print(list(le.inverse_transform([0, 1, 2, 3])))
