# import the necessary packages
import os
from src.constants import *

# Model-specific
# VGG16
from tensorflow.keras.applications import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg16_preprocess_input
# Inception V3
from tensorflow.keras.applications import InceptionV3
from tensorflow.keras.applications.inception_v3 import preprocess_input as inception_v3_preprocess_input

from tensorflow.keras.applications import InceptionResNetV2
from tensorflow.keras.applications.inception_resnet_v2 import preprocess_input as inception_resnet_v2_preprocess_input

# initialize the path to the *original* input directory of images
set_name = '4-classes-ccoo5-20211114-135918'
ORIG_INPUT_DATASET = os.path.join(TRAINING_VALIDATION_INPUT_DIR, set_name)

# initialize the base path to the *new* directory that will contain
# our images after computing the training and testing split
BASE_PATH = "dataset" + '-' + set_name

# define the names of the training, testing, and validation
# directories
TRAIN = "training"
TEST = "evaluation"
VAL = "validation"

# initialize the list of class label names
# CLASSES = ['closed', 'open']
# CLASSES = ['almost_closed', 'closed', 'open']
CLASSES = ['closed', 'closing', 'open', 'opening']

# set the batch size
BATCH_SIZE = 32

# Feature extraction model
# load the network and initialize the label encoder
print("[INFO] loading network...")
# VGG16
model = VGG16(weights="imagenet", include_top=False)
preprocess_input = vgg16_preprocess_input
shape = 7 * 7 * 512
# Inception V3
# model = InceptionV3(weights="imagenet", include_top=False)
# preprocess_input = inception_v3_preprocess_input
# shape = 5 * 5 * 2048
# InceptionResNetV2
# model = InceptionResNetV2(weights="imagenet", include_top=False)
# preprocess_input = inception_resnet_v2_preprocess_input
# shape = 5 * 15 * 512 # 38,400

# initialize the label encoder file path and the output directory to
# where the extracted features (in CSV file format) will be stored
OUTPUT = "output" + '-' + set_name + '-' + model.name
LE_PATH = os.path.sep.join([OUTPUT, "le.cpickle"])
BASE_CSV_PATH = OUTPUT

# set the path to the serialized model after training
MODEL_PATH = os.path.sep.join([OUTPUT, "model.cpickle"])
