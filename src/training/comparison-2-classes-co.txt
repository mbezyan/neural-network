For all of below D:\Programs\Python\Python39\python.exe was used to execute the Python script
------------------------------------------------------------------------------------------------------------------------
Notes:
From this point onwards:
- Training sets: 01, 02, 03, 05, 07, 08, 09, 10, 11, 12, 13, 15, 17
- Validation sets: 04, 06, 14
- Evaluation sets: 16, 18, 19
------------------------------------------------------------------------------------------------------------------------
D:/comp8800/mediapipe/python/python-mediapipe1/src/training/transfer_learning_keras_feature_extraction/train.py
Set name:2-classes-co-20211009-221104
Classes: ['closed', 'open']
[INFO] evaluating...
              precision    recall  f1-score   support

      closed       0.89      0.99      0.93       969
        open       0.99      0.88      0.93      1016

    accuracy                           0.93      1985
   macro avg       0.94      0.93      0.93      1985
weighted avg       0.94      0.93      0.93      1985
------------------------------------------------------------------------------------------------------------------------
D:/comp8800/mediapipe/python/python-mediapipe1/src/training/transfer_learning_keras_fine_tuning/train.py
Set name:2-classes-co-20211009-221104
Classes: ['closed', 'open']
Found 12602 images belonging to 2 classes.
Found 1598 images belonging to 2 classes.
Found 1985 images belonging to 2 classes.
[INFO] training head...
Epoch 10/10
393/393 [==============================] - 190s 482ms/step - loss: 0.1162 - accuracy: 0.9550 - val_loss: 0.1750 - val_accuracy: 0.9343
[INFO] evaluating after fine-tuning network head...
              precision    recall  f1-score   support

      closed       0.95      0.95      0.95       969
        open       0.95      0.95      0.95      1016

    accuracy                           0.95      1985
   macro avg       0.95      0.95      0.95      1985
weighted avg       0.95      0.95      0.95      1985
[INFO] re-compiling model...
Epoch 4/4
393/393 [==============================] - 210s 534ms/step - loss: 0.0768 - accuracy: 0.9718 - val_loss: 0.1163 - val_accuracy: 0.9547
[INFO] evaluating after fine-tuning network...
              precision    recall  f1-score   support

      closed       0.97      0.97      0.97       969
        open       0.97      0.97      0.97      1016

    accuracy                           0.97      1985
   macro avg       0.97      0.97      0.97      1985
weighted avg       0.97      0.97      0.97      1985
------------------------------------------------------------------------------------------------------------------------
D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/training/transfer_learning_keras_fine_tuning/train.py
Set name:2-classes-co-20211009-221104
Classes: ['closed', 'open']
Found 12602 images belonging to 2 classes.
Found 1598 images belonging to 2 classes.
Found 1985 images belonging to 2 classes.
[INFO] training head...
Epoch 20/20
393/393 [==============================] - 192s 488ms/step - loss: 0.0957 - accuracy: 0.9621 - val_loss: 0.1625 - val_accuracy: 0.9381
[INFO] evaluating after fine-tuning network head...
              precision    recall  f1-score   support

      closed       0.95      0.97      0.96       969
        open       0.97      0.95      0.96      1016

    accuracy                           0.96      1985
   macro avg       0.96      0.96      0.96      1985
weighted avg       0.96      0.96      0.96      1985
[INFO] re-compiling model...
Epoch 8/8
393/393 [==============================] - 217s 551ms/step - loss: 0.0591 - accuracy: 0.9768 - val_loss: 0.1196 - val_accuracy: 0.9560
[INFO] evaluating after fine-tuning network...
              precision    recall  f1-score   support

      closed       0.97      0.98      0.98       969
        open       0.98      0.98      0.98      1016

    accuracy                           0.98      1985
   macro avg       0.98      0.98      0.98      1985
weighted avg       0.98      0.98      0.98      1985
------------------------------------------------------------------------------------------------------------------------
D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/training/transfer_learning_keras_fine_tuning/train.py
Set name:2-classes-co-20211009-221104
Classes: ['closed', 'open']
Found 12602 images belonging to 2 classes.
Found 1598 images belonging to 2 classes.
Found 1985 images belonging to 2 classes.
[INFO] training head...
Epoch 50/50
393/393 [==============================] - 183s 464ms/step - loss: 0.0797 - accuracy: 0.9692 - val_loss: 0.1836 - val_accuracy: 0.9216
[INFO] evaluating after fine-tuning network head...
              precision    recall  f1-score   support

      closed       0.95      0.98      0.96       969
        open       0.98      0.95      0.96      1016

    accuracy                           0.96      1985
   macro avg       0.96      0.96      0.96      1985
weighted avg       0.96      0.96      0.96      1985
[INFO] re-compiling model...
Epoch 20/20
393/393 [==============================] - 193s 491ms/step - loss: 0.0390 - accuracy: 0.9849 - val_loss: 0.1009 - val_accuracy: 0.9598
[INFO] evaluating after fine-tuning network...
              precision    recall  f1-score   support

      closed       0.95      0.98      0.97       969
        open       0.98      0.95      0.97      1016

    accuracy                           0.97      1985
   macro avg       0.97      0.97      0.97      1985
weighted avg       0.97      0.97      0.97      1985
------------------------------------------------------------------------------------------------------------------------
***********
Note: from this point onwards is for dataset that was created from fixed grayscale images where closed eyes are
considered to be those where the eye is fully closed or where the pupil is behind the eyelid.
***********
------------------------------------------------------------------------------------------------------------------------
D:/comp8800/mediapipe/python/python-mediapipe1/src/training/transfer_learning_keras_feature_extraction/train.py
Set name:2-classes-co-20211017-214639
Classes: ['closed', 'open']
              precision    recall  f1-score   support

      closed       0.97      0.99      0.98       664
        open       0.99      0.97      0.98       707

    accuracy                           0.98      1371
   macro avg       0.98      0.98      0.98      1371
weighted avg       0.98      0.98      0.98      1371
------------------------------------------------------------------------------------------------------------------------
Notes:
From this point onwards:
- Training sets: 01, 02, 03, 05, 07, 08, 09, 10, 11, 12, 13, 15, 17, 23, 24
- Validation sets: 04, 06, 14, 25
- Evaluation sets: 16, 18, 19, 26
------------------------------------------------------------------------------------------------------------------------
D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/training/transfer_learning_keras_feature_extraction/train.py
Set name:2-classes-co-20211106-215245
Classes: ['closed', 'open']
              precision    recall  f1-score   support

      closed       0.97      0.90      0.94      1395
        open       0.91      0.97      0.94      1418

    accuracy                           0.94      2813
   macro avg       0.94      0.94      0.94      2813
weighted avg       0.94      0.94      0.94      2813
------------------------------------------------------------------------------------------------------------------------
Notes:
From this point onwards:
- Training sets: 01, 02, 03, 05, 07, 08, 09, 10, 11, 12, 13, 15, 17, 23, 24, 27, 28
- Validation sets: 04, 06, 14, 25
- Evaluation sets: 16, 18, 19, 26
------------------------------------------------------------------------------------------------------------------------
D:\Programs\Python\Python39\python.exe D:/comp8800/mediapipe/python/python-mediapipe1/src/training/transfer_learning_keras_feature_extraction/train.py
Set name:2-classes-co-20211107-221604
Classes: ['closed', 'open']
Feature extraction model: vgg16
              precision    recall  f1-score   support

      closed       0.97      0.94      0.96      1395
        open       0.94      0.98      0.96      1459

    accuracy                           0.96      2854
   macro avg       0.96      0.96      0.96      2854
weighted avg       0.96      0.96      0.96      2854
------------------------------------------------------------------------------------------------------------------------