from typing import Final
import os
from src.common.classification_type import ClassificationType
import timeit
import os
from src.common.util.logging_util import logger

__start = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': Started @ ' + str(__start))

ROOT_DIR: Final = os.path.join(os.path.dirname(__file__), '..')

# Output directories
OUTPUT_DIR: Final = os.path.join(ROOT_DIR, 'out')
IMAGES_OUTPUT_DIR: Final = os.path.join(OUTPUT_DIR, 'images')
CSV_OUTPUT_DIR: Final = os.path.join(OUTPUT_DIR, 'csv')
MODEL_OUTPUT_DIR: Final = os.path.join(OUTPUT_DIR, 'models')
TEMP_OUTPUT_DIR: Final = os.path.join(OUTPUT_DIR, 'tmp')

# Input directories
INPUT_DIR: Final = os.path.join(ROOT_DIR, 'input')
VIDEOS_INPUT_DIR: Final = os.path.join(INPUT_DIR, 'videos')
TRAINING_VALIDATION_INPUT_DIR = os.path.join(INPUT_DIR, 'training-validation')
LAB_SESSION_VIDEO_DIR = os.path.join(ROOT_DIR, '..', 'mind-lab-study-api1', 'upload')

# Training directory
TRAINING_DIR: Final = os.path.join(ROOT_DIR, 'src', 'training')

# Dimensions for scaling
MIN_DIMENSIONS_FOR_GOOGLENET = (224, 224)

number_of_imgs_to_stack: Final = 3
frame_rate: Final = 30
seconds_in_minute: Final = 60

CLASSES = {
    ClassificationType.CCOO_4: ['closed', 'closing', 'open', 'opening'],
    ClassificationType.ACO_3: ['almost_closed', 'closed', 'open'],
    ClassificationType.CO_2: ['closed', 'open'],
}

# Videos
videos: Final = {'01-asian-female':
                     {'extension': 'mp4', 'img_name_prefix': 'asfe', 'classified_imgs_dir': '20210814-161723'},
                 '02-australian-white-male1-M0gGLCXR1n0':
                     {'extension': 'mp4', 'img_name_prefix': 'auwm', 'classified_imgs_dir': '20210814-223757'},
                 '03-black-female1-k2jiK8zWsb0':
                     {'extension': 'mp4', 'img_name_prefix': 'blfe', 'classified_imgs_dir': '20210814-173924'},
                 '04-black-female2-SskrmHCoiRc':
                     {'extension': 'mp4', 'img_name_prefix': 'blfm', 'classified_imgs_dir': '20210814-223909'},
                 '05-black-male-news-reader-XMrxsEDtyTQ':
                     {'extension': 'mp4', 'img_name_prefix': 'bmnr', 'classified_imgs_dir': '20210814-171135'},
                 '06-blond-white-male-uwPtFbA1sRY':
                     {'extension': 'mp4', 'img_name_prefix': 'blwm', 'classified_imgs_dir': '20210814-174158'},
                 '07-caucasian-male1-youtube-_hjfQ41LF0w':
                     {'extension': 'mp4', 'img_name_prefix': 'cmyt', 'classified_imgs_dir': '20210814-161629'},
                 '08-caucasian-male-news-reader-wZNng505VYM':
                     {'extension': 'mp4', 'img_name_prefix': 'cmnr', 'classified_imgs_dir': '20210814-165418'},
                 '09-eastern-european-female-youtube-Tjom_yAUj-s':
                     {'extension': 'mp4', 'img_name_prefix': 'eefy', 'classified_imgs_dir': '20210814-163347'},
                 '10-indian-female1-ZD6jw2FvWaE':
                     {'extension': 'mp4', 'img_name_prefix': 'infe', 'classified_imgs_dir': '20210814-224005'},
                 '11-indian-male1-c9F5kMUfFKk':
                     {'extension': 'mp4', 'img_name_prefix': 'inma', 'classified_imgs_dir': '20210814-224222'},
                 '12-man-glasses-monitor-reflection':
                     {'extension': 'mp4', 'img_name_prefix': 'mgmr', 'classified_imgs_dir': '20210814-161510'},
                 '13-mohammad-reading':
                     {'extension': 'mp4', 'img_name_prefix': 'more', 'classified_imgs_dir': '20210814-161339'},
                 '14-white-female-news-interview-XMrxsEDtyTQ':
                     {'extension': 'mp4', 'img_name_prefix': 'wfni', 'classified_imgs_dir': '20210814-170621'},
                 '15-white-male-bald-round-face-uwPtFbA1sRY':
                     {'extension': 'mp4', 'img_name_prefix': 'wmbr', 'classified_imgs_dir': '20210814-174232'},
                 '16-african-canadian-french-female-glasses-mGWzHsgld58':
                     {'extension': 'mp4', 'img_name_prefix': '16bf', 'classified_imgs_dir': '20210913-064152'},
                 '17-cacusian-male-EACDJJW3aqo':
                     {'extension': 'mp4', 'img_name_prefix': '17cm', 'classified_imgs_dir': '20210914-165324'},
                 '18-australia-white-male-glasses-Y6QtKgB8K3Q':
                     {'extension': 'mp4', 'img_name_prefix': '18wm', 'classified_imgs_dir': '20210916-222353'},
                 '19-indian-male-5j7KxVTNBdY':
                     {'extension': 'mp4', 'img_name_prefix': '19im', 'classified_imgs_dir': '20210918-172909'},
                 '20-white-obese-male1-TQHW7gGjrCQ':
                     {'extension': 'mp4', 'img_name_prefix': '20wm', 'classified_imgs_dir': '20211009-110725'},
                 '21-hispanic-female1-Qlu_7ZcbSW0':
                     {'extension': 'mp4', 'img_name_prefix': '21hf', 'classified_imgs_dir': '20211009-140747'},
                 '22-old-white-female1-Qlu_7ZcbSW0':
                     {'extension': 'mp4', 'img_name_prefix': '22wf', 'classified_imgs_dir': ''},
                 '23-caucasian-american-female1-aIvRs-M8n8o':
                     {'extension': 'mp4', 'img_name_prefix': '23cf', 'classified_imgs_dir': '20211106-102354'},
                 '24-caucasian-american-female2-aIvRs-M8n8o':
                     {'extension': 'mp4', 'img_name_prefix': '24cf', 'classified_imgs_dir': '20211106-103259'},
                 '25-white-german-male1-7YHZ2qXXPxw':
                     {'extension': 'mp4', 'img_name_prefix': '25wm', 'classified_imgs_dir': '20211106-104135'},
                 '26-while-female-eye-makeup2-eE1kAY87UbM':
                     {'extension': 'mp4', 'img_name_prefix': '26wf', 'classified_imgs_dir': '20211106-104653'},
                 '27-caucasian-male2-XCjosBT3Gy8':
                     {'extension': 'mp4', 'img_name_prefix': '27cm', 'classified_imgs_dir': '20211107-155821'},
                 '28-caucasian-middle-aged-male-X2mVL_trjmo':
                     {'extension': 'mp4', 'img_name_prefix': '28cm', 'classified_imgs_dir': '20211107-162033'},
                 '29-caucasian-young-male-angle1-zgwMnvPbzuQ':
                     {'extension': 'mp4', 'img_name_prefix': '29cm', 'classified_imgs_dir': '20211113-113453'},
                 '30-asian-female2-kayOhGRcNt4':
                     {'extension': 'mp4', 'img_name_prefix': '30af', 'classified_imgs_dir': '20211113-114406'},
                 '31-cacucasian-male-bald-glasses1-wIjK-6Do6lg':
                     {'extension': 'mp4', 'img_name_prefix': '31cm', 'classified_imgs_dir': '20211113-135634'},
                 '32-caucasian-young-male2-oocNVvTHP5M':
                     {'extension': 'mp4', 'img_name_prefix': '32cm', 'classified_imgs_dir': '20211113-142354'},
                 '33-indian-male-glasses1-DqNb0png0gA':
                     {'extension': 'mp4', 'img_name_prefix': '33im', 'classified_imgs_dir': '20211113-143003'},
                 '34-caucasian-middle-aged-female-light-makeup1-oYFCbCZF8hA':
                     {'extension': 'mp4', 'img_name_prefix': '34cf', 'classified_imgs_dir': '20211114-100832'},
                 '35-caucasian-old-american-male1-eI65CbmTlAc':
                     {'extension': 'mp4', 'img_name_prefix': '35cm', 'classified_imgs_dir': '20211114-104007'},
                 '36-white-female2-3bTZgO4aAhI':
                     {'extension': 'mp4', 'img_name_prefix': '36wf', 'classified_imgs_dir': '20211114-104007'},
                 '37-white-female3-nuxzL6f2SCU':
                     {'extension': 'mp4', 'img_name_prefix': '37wf', 'classified_imgs_dir': '20211114-104007'},
                 '38-black-female3-VxulD6o1a5g':
                     {'extension': 'mp4', 'img_name_prefix': '38bf', 'classified_imgs_dir': '20211114-104007'},
                 '26122013_223310_cam':
                     {'extension': 'avi', 'img_name_prefix': 'whfe', 'classified_imgs_dir': ''},
                 'talking':
                     {'extension': 'avi', 'img_name_prefix': 'whma', 'classified_imgs_dir': ''},
                 '26122013_230103_cam':
                     {'extension': 'avi', 'img_name_prefix': 'whm2', 'classified_imgs_dir': ''},
                 'mohammad-iq-test-1-of-3':
                     {'extension': 'mp4', 'img_name_prefix': 'irm2', 'classified_imgs_dir': ''},
                 'Video 32':
                     {'extension': 'mp4', 'img_name_prefix': 'asma', 'classified_imgs_dir': ''},
                 'mmb1':
                     {'extension': 'webm', 'img_name_prefix': 'irm3', 'classified_imgs_dir': ''},
                 'mmb2':
                     {'extension': 'webm', 'img_name_prefix': 'irm4', 'classified_imgs_dir': ''},
                 'mmb3':
                     {'extension': 'webm', 'img_name_prefix': 'irm5', 'classified_imgs_dir': ''},
                 'mmb4':
                     {'extension': 'webm', 'img_name_prefix': 'irm6', 'classified_imgs_dir': ''},
                 'mmb5':
                     {'extension': 'webm', 'img_name_prefix': 'irm7', 'classified_imgs_dir': ''},
                 'mmb6':
                     {'extension': 'mp4', 'img_name_prefix': 'irm7', 'classified_imgs_dir': ''},
                 '4f07740eeabcef58bc3d2ec242d827ba':
                     {'extension': None, 'img_name_prefix': 'irm8', 'classified_imgs_dir': ''},
                 '4cd8a04e8c1dec47507a3cbc993db11b':
                     {'extension': None, 'img_name_prefix': 'irm9', 'classified_imgs_dir': ''},
                 'f90320ba8018d09fe6ab874dd903a9f7':
                     {'extension': None, 'img_name_prefix': 'irm10', 'classified_imgs_dir': ''}}


__stop = timeit.default_timer()
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ُStopped @ ' + str(__stop))
logger().debug("[PROFILING] " + os.path.abspath(__file__) + ': ' + str(__stop - __start))
