import numpy as np
from scipy import signal
import os
import cv2
from scipy.signal import lfilter
from src import constants
from classfication_util import cnn_logistic_regression_classify, cnn_classify
import src.common.util.utils as util
# from common.custom_exceptions import *
from src.common.custom_exceptions import *
import logging
from typing import Final
from tensorflow.keras.preprocessing.image import load_img

# Set up logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
console_log_handler = logging.StreamHandler()
console_log_handler.setLevel(logging.DEBUG)
console_log_handler.setFormatter(formatter)
logger.addHandler(console_log_handler)

MODEL_FOR_CLASSIFICATION: Final = 'co2-logistic-regression'

def normalise(list_to_normalise):
    min_value = min(list_to_normalise)
    max_value = max(list_to_normalise)
    diff = max_value - min_value
    return [(x - min_value) / diff for x in list_to_normalise]


# https://www.kite.com/python/answers/how-to-find-the-local-extrema-of-a-numpy-array-in-python
def local_minima(source_list):
    np_array = np.array(source_list)
    return signal.argrelextrema(np_array, np.less)


def local_maxima(source_list):
    np_array = np.array(source_list)
    return signal.argrelextrema(np_array, np.greater)


def smooth_curve(data):
    n = 10  # the larger n is, the smoother curve will be
    b = [1.0 / n] * n
    a = 1
    return lfilter(b, a, data)


def calculate_eye_states(eye_areas):
    normalised_areas = normalise(eye_areas)
    # smooth_areas = smooth_curve(normalised_areas)
    normalised_areas_minima_indices = local_minima(normalised_areas)
    normalised_areas_maxima_indices = local_maxima(normalised_areas)
    eye_states = {}
    for j in range(len(normalised_areas)):
        if j in np.asarray(normalised_areas_minima_indices):
            eye_closed_open = 0
        elif j in np.asarray(normalised_areas_maxima_indices):
            eye_closed_open = 1
        else:
            eye_closed_open = None
        eye_states[j + 1] = [normalised_areas[j], eye_closed_open, None]

    return eye_states


def extract_classify_eyes(video_name):
    # Create directory for output
    file_dir_name = video_name + "-" + util.timestamp()
    images_dir_name = os.path.join(constants.IMAGES_OUTPUT_DIR, file_dir_name)
    os.mkdir(images_dir_name)
    os.mkdir(os.path.join(images_dir_name, 'closed'))
    os.mkdir(os.path.join(images_dir_name, 'open'))

    video_capture_handle = util.video_capture_handle_for_name(video_name)

    frame = 0
    # For each frame
    while True:
        frame += 1

        # Image
        ret, image = video_capture_handle.read()

        # When ret is False, it means there are no more frames
        if ret is False:
            logger.info("Frame " + str(frame-1) + " was the last frame.")
            break

        try:
            left_eye, right_eye = util.extract_LEEB_REEB_from_image(image)[0:2]
            # classification = cnn_classify(right_eye, MODEL_FOR_CLASSIFICATION)[1]
            image_path = os.path.join(images_dir_name, constants.videos[video_name]['img_name_prefix'] + '-right-' + str(frame).zfill(5) + '.png')
            cv2.imwrite(image_path, right_eye)
            right_eye_for_classification = load_img(image_path, target_size=(224, 224))
            classification = cnn_logistic_regression_classify(right_eye_for_classification, MODEL_FOR_CLASSIFICATION)[1]
            cv2.imwrite(os.path.join(images_dir_name, classification, constants.videos[video_name]['img_name_prefix'] + '-right-' + str(frame).zfill(5) + '.png'), right_eye)
            # classification = cnn_classify(left_eye, MODEL_FOR_CLASSIFICATION)[1]
            image_path = os.path.join(images_dir_name, constants.videos[video_name]['img_name_prefix'] + '-left-' + str(frame).zfill(5) + '.png')
            cv2.imwrite(image_path, left_eye)
            left_eye_for_classification = load_img(image_path, target_size=(224, 224))
            classification = cnn_logistic_regression_classify(left_eye_for_classification, MODEL_FOR_CLASSIFICATION)[1]
            cv2.imwrite(os.path.join(images_dir_name, classification, constants.videos[video_name]['img_name_prefix'] + '-left-' + str(frame).zfill(5) + '.png'), left_eye)
        except NoFacialLandmarksFound:
            # Go to next frame and skip this one
            logger.warning("Frame " + str(frame) + " skipped because no landmarks were found")
            continue
        except LeftEyeEyebrowNotFound:
            logger.warning("Frame " + str(frame) + " skipped because left eye was not detected")
            continue
        except RightEyeEyebrowNotFound:
            logger.warning("Frame " + str(frame) + " skipped because right eye was not detected")
            continue

        if frame % 300 == 0:
            print(str(frame))


extract_classify_eyes('35-caucasian-old-american-male1-eI65CbmTlAc')
